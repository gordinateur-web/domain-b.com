<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title>Research Report | Domain-b </title>
      <?php include("head.php"); ?>
     
   </head>
   <body class="">
      <!-- Body start -->
      <?php include("header.php"); ?>
     	

      <!--login window start here-->
	  	<div class="container ads-container">
	  		<div class="row">
	  			<div class="banner-img">
	  					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	  						<div class="research-report-heading">
	  							<h1 class="page-heading">Browse by categories</h1>
	  						</div>
	  				</div>
	  			</div>
	  		</div>
	  		<div class="row">
	  			<!--categories 1 col start here-->
		  			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
		  				<div class="category-listing">
		  					<h4 class="categories-heading">Pharmaceuticals &amp; Healthcare</h4> 
		  					<ul class="list-inline category-list">
		  						<li>Biotechnology</li>
		  						<li>Healthcare and Medical Devices</li>
		  						<li>Pharmaceuticals</li>
		  					</ul>
		  				</div>
		  				<div class="category-listing">
		  					<h4 class="categories-heading">Business and Finance </h4> 
		  					<ul class="list-inline category-list">
		  						<li>Business</li>
		  						<li> Banking and Financial Services</li>
		  					</ul>
		  				</div>
		  				<div class="category-listing">
		  					<h4 class="categories-heading">Retail</h4> 
		  					<ul class="list-inline category-list">
		  						<li>Retail</li>
		  						<li>Consumer goods</li>
		  						<li>Food &amp; Beverage</li>
		  					</ul>
		  				</div>
		  				<div class="category-listing">
		  					<h4 class="categories-heading">Industry</h4> 
		  					<ul class="list-inline category-list">
		  						<li>Agriculture</li>
		  						<li>Construction</li>
		  						<li>Energy</li>
		  						<li>Metallurgy</li>
		  						<li>Mineral raw materials</li>
		  						<li>Machinery &amp; equipment</li>
		  						<li>Defence</li>
		  						<li>Packaging</li>
		  						<li>Publishing</li>
		  						<li>Pulp  &amp; paper</li>
		  						<li>Vehicle</li>
		  					</ul>
		  				</div>
		  				
		  			</div>
		  		<!--categories end here-->

		  		<!--categories 2 col start here-->
		  			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
		  				<div class="category-listing">
		  					<h4 class="categories-heading">Chemicals</h4> 
		  					<ul class="list-inline category-list">
		  						<li>Organic Chemicals </li>
		  						<li>Inorganic Chemicals</li>
		  						<li>Chemical Company </li>
		  						<li>Reports  </li>
		  						<li>Chemical Reports by CAS </li>
		  						<li>Number  </li>
		  						<li>Composite Materials  </li>
		  						<li>Oils &amp; Lubricants</li>
		  					</ul>
		  				</div>
		  				<div class="category-listing">
		  					<h4 class="categories-heading">IT &amp; Technology</h4> 
		  					<ul class="list-inline category-list">
		  						<li>Consumer Electronics</li>
		  						<li> Hardware </li>
		  						<li> Software </li>
		  						<li> Telecommunications </li>
		  					</ul>
		  				</div>
		  				
		  				<div class="category-listing">
		  					<h4 class="categories-heading">Life sciences</h4> 
		  					<ul class="list-inline category-list">
		  						<li>Medical Devices</li>
		  						<li>Biotechnology</li>
		  						<li>Diagnostics &amp; Diseases</li>
		  						<li>Healthcare</li>
		  						<li>Pharmaceuticals</li>
		  						<li>Medical Products </li>
		  						<li>Veterinary</li>
		  					</ul>
		  				</div>
		  			</div>
		  		<!--categories end here-->

		  		<!--categories 3 col start here-->
		  			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
		  				<div class="category-listing">
		  					<h4 class="categories-heading">Services</h4> 
		  					<ul class="list-inline category-list">
		  						<li>Freight &amp; Trucking</li>
		  						<li>Media &amp; Entertainment</li>
		  						<li>Food Service</li>
		  						<li>Consumer Services </li>
		  						<li>Education </li>
		  						<li>Hotels </li>
		  						<li>Legal Services </li>
		  						<li>Logistics </li>
		  						<li>Post &amp; Courier </li>
		  						<li>Recruitment </li>
		  						<li>Rent </li>
		  						<li>Sports &amp; Fitness Center </li>
		  						<li>Travel &amp; Leisure </li>
		  						<li>Other Services</li>
		  					</ul>
		  				</div>
		  				<div class="subscribe-section">
		  					<p class="subscribe-title">Subscribe to updates</p>
		  					<p class="subscribe-text">Keep up with our lates research reports</p>
		  					 <input type="email" class="form-control  your-details" id="email" placeholder="Enter email address">
		  					 <button type="button" class="btn btn-default green-button">SUBSCRIBE <span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span> </button>
		  				</div>
		  			</div>
		  		<!--categories end here-->
		  		<!--start add section-->
					<div class="adds-section research-page right hidden-laptop hidden-md hidden-xs">
						<div class="adds-img">
							<a href="#"><img src="images/adds-report1.png" alt="add-1" class="img-responsive"> </a>
						</div>
						<div class="adds-img">
							<a href="#"><img src="images/adds-report.png" alt="add-1" class="img-responsive"> </a>
						</div>
					</div>
				<!--end add section -->
				
		  	</div>
		  	<section class="advertise-section">
							<a href="#"><img src="images/advertise-img.jpg" class="img-responsive"></a>
						</section>

		  			

		  	<!--footer online icons-->
		  	<div class="row online-icon-section">
	  			<div class="col-lg-6 col-sm-4 col-xs-6 pull-left ">
	  				<div class="secured-img"><a href="#"><img src="images/secured-img.png" alt="secured-by" class="img-responsive"></a></div>
	  			</div>
	  			<div class="col-lg-6 col-sm-8 col-xs-12">
					<div class="row">
						<div class="col-lg-7 col-md-6 col-sm-7 col-xs-8">
							<p class="credit-card-text">We accept all major credit cards!</p>
							<ul class="list-inline">
								<li><a href="#"><img src="images/icon_1.jpg" alt="secured-by" class="img-responsive"></a></li>
								<li><a href="#"><img src="images/icon_2.jpg" alt="secured-by" class="img-responsive"></a></li>
								<li><a href="#"><img src="images/icon_3.jpg" alt="secured-by" class="img-responsive"></a></li>
								<li><a href="#"><img src="images/icon_4.jpg" alt="secured-by"class="img-responsive"></a></li>
								<li><a href="#"><img src="images/icon_5.jpg" alt="secured-by" class="img-responsive"></a></li>
							</ul>
						</div>
						<div class="col-lg-5 col-md-6 col-sm-5 col-xs-4">
							<p class="credit-card-text">No PayPal account required</p>
							<div class="paypal-img">
								<a href="#"><img src="images/icon_6.jpg" alt="secured-by" class="img-responsive"></a>
							</div>
						</div>
				  	</div>
				</div>
		  	</div>
		  	<!--footer online icons end here -->
	  	</div>
	  	<!--end login here-->
	  	
      <?php include("footer.php"); ?>
      <?php include("script.php"); ?>
	   <script>
			$('.alert-item').imagesLoaded( function() {
				$('.alert-item').masonry({
				  itemSelector: '.alert-box',
				});

				setTimeout(function() {
					$('.alert-item').masonry('layout');
				},500);
			});

		</script>
	  
   </body>
   <!-- Body end -->
</html>