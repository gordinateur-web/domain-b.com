$(document).ready(function() {
	$('button[data-toggle="collapse"]').click(function() {
		$(this).toggleClass('active');
	});
		// Add slideDown animation to Bootstrap dropdown when expanding.
	 
// Add slideDown animation to Bootstrap dropdown when expanding.
  $('.dropdown').on('show.bs.dropdown', function() {
    $(this).find('.dropdown-menu').first().stop(true, true).slideDown();
  });

  // Add slideUp animation to Bootstrap dropdown when collapsing.
  $('.dropdown').on('hide.bs.dropdown', function() {
    $(this).find('.dropdown-menu').first().stop(true, true).slideUp();
  });

//touchswipe
$(".carousel-inner").swipe( {
	//Generic swipe handler for all directions
	swipeLeft:function(event, direction, distance, duration, fingerCount) {
		$(this).parent().carousel('next'); 
	},
	swipeRight: function() {
		$(this).parent().carousel('prev'); 
	},
	//Default is 75px, set to 0 for demo so any distance triggers swipe
	threshold:0
});

});
$(document).ready(function() {
	// multi-level navbar for mobile
    $('.navbar a.dropdown-toggle').on('click', function(e) {
        var $el = $(this);
        var $parent = $(this).offsetParent(".dropdown-menu");
        $(this).parent("li").toggleClass('open');

        if(!$parent.parent().hasClass('nav')) {
            $el.next().css({"top": $el[0].offsetTop, "left": $parent.outerWidth() - 4});
        }

        $('.nav li.open').not($(this).parents("li")).removeClass("open");

        return false;
    });
	
});


//Tab auto shift js
	var tabChange = function () {
    var tabs = $('.nav-pills > li');
    var active = tabs.filter('.active');
    var next = active.next('li').length ? active.next('li').find('a') : tabs.filter(':first-child').find('a');
    // Use the Bootsrap tab show method
    next.tab('show');
	};
// Tab Cycle function
	var tabCycle = setInterval(tabChange, 8000);

// Tab click event handler
	$('.tabs-autoslide a').on('click', function (e) {
    e.preventDefault();
    // Stop the cycle
    clearInterval(tabCycle);
    // Show the clicked tabs associated tab-pane
    $(this).tab('show');
    // Start the cycle again in a predefined amount of time
    setTimeout(function () {
        //tabCycle = setInterval(tabChange, 5000);
    }, 15000);
});

// option value color js
$(function(){	
	$('#select-country').focusin(function(){
		 $(this).css('color','#bcbcbc')
       }); 	 
	$('#select-country').focusout(function(){		  
		if(this.value === '') $(this).css('color','#bcbcbc'); else $(this).css('color','#bcbcbc');			
       });
 });	
 

//offset dropdown
/* Define dropdown position right left on hover of menu parent 
$('.show-on-hover > .dropdown-toggle:first-child').hover(function(ele) {
	var dropdown_list = $('.dropdown-menu');
	var dropdown_offset = $(this).offset();
	var offsetLeft = dropdown_offset.left;
	var dropdown_width = dropdown_list.width();
	var doc_width = $(window).width();

	var isDropdownVisible = (offsetLeft + dropdown_width <= doc_width);
	var isSubDropdownVisible = (offsetLeft + (dropdown_width*2) <= doc_width);

	if (!isDropdownVisible || !isSubDropdownVisible) {
		//$('.dropdown-menu').addClass('dropdown-submenu-right');
		$(ele).parent().find('.dropdown-menu').addClass('dropdown-submenu-right');
		$(ele).parent().find('.dropdown-menu')[0].removeClass('dropdownparent-right').addClass('dropdownparent-right');


	} else {
		//$('.dropdown-menu').removeClass('dropdown-submenu-right');
		$('.dropdown-menu').addClass('dropdown-submenu-right');
		$('.dropdown-menu')[0].removeClass('dropdownparent-right').addClass('dropdownparent-right');
	}
});*/