<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title>Research  | Domain-b </title>
      <?php include("head.php"); ?>
     
   </head>
   <body class="">
      <!-- Body start -->
      <?php include("header1.php"); ?>
     	 

      <!--login window start here-->
	  	<div class="container push-footer-search ads-container">
	  		<div class="row">
	  			<div class="banner-img">
	  				<div class="row">
			  			<div class="col-lg-2 col-md-5 col-sm-6 hidden-xs">
			  				<h1 class="page-heading search">SEARCH</h1>
			  			</div>
					</div>
					<div class="row">
							<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
								<div class="blog-search">
									<h4 class="blog-title-search">INDUSTRY STANDARDS</h4>
									<p class="blog-text-search">Domain-b Research assists research 
									professionals in the development of best industry standards and professional practices.</p>
								</div>
							</div>
							
							<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
								<div class="blog-search">
									<h4 class="blog-title-search">TRUSTED BY LEADING COMPANIES</h4>
									<p class="blog-text-search">Top executives from leading organizations purchase research reports from us.</p>
								</div>
							</div>
							
							<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
								<div class="blog-search">
									<h4 class="blog-title-search">SECURE CHECKOUT</h4>
									<p class="blog-text-search">Shop hasselfree about safety &amp; security of your transactions</p>
								</div>
							</div>
					</div>
	  			</div>
	  		</div>
	  		<div class="row">
	  			<h2 class="login-maintitle">Browse by categories</h2>
	  			<p class="research-text">We provide thousands of reports, books, journals and other information products, covering the latest and biggest issues in all industry sectors. Please browse our categories below.</p>
	  			<p class="research-text heading">EXPAND ALL CATEGORIES <span class="glyphicon glyphicon glyphicon-menu-right" aria-hidden="true"></span></p>
	  			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	  			<!--accordion start-->
	  				<div class="panel-group" id="accordion">
						  <div class="panel panel-default">
						    <div class="panel-heading">
						      <h4 class="panel-title search">
						        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
						          Pharmaceuticals and Healthcare <span class="text-view-all pull-right">View all</span>
						        </a>
						      </h4>
						    </div>
						    <div id="collapseOne" class="panel-collapse collapse in">
						      <div class="panel-body">
						        	<div class="category-listing">
					  					<h4 class="categories-heading">Biotechnology 
					  						<span class="text-view-product pull-right">VIEW ALL PRODUCTS IN THIS CATEGORY</span>

					  					</h4> 
					  					<ul class="list-inline category-list">
					  						<li>Genomics</li>
					  						<li>Markets</li>
					  						<li>Technology</li>
					  						<li>Fast Facts</li>
					  						<li>Companies</li>
					  						<li>Proteomics</li>
					  						<li>Bioinformatics</li>
					  					</ul>
					  				</div>
					  				<div class="category-listing">
					  					<h4 class="categories-heading">Pharmaceuticals   
					  						<span class="text-view-product pull-right">VIEW ALL PRODUCTS IN THIS CATEGORY</span>
					  					</h4> 
					  					<ul class="list-inline category-list">
					  						<li>Companies</li>
					  						<li> Drug Delivery</li>
					  						<li>Markets</li>
					  						<li> Fast Facts</li>
					  						<li>Companies</li>
					  						<li> Proteomics</li>
					  					</ul>
					  				</div>
						      </div>
						    </div>
						  </div>
						  <div class="panel panel-default">
						    <div class="panel-heading">
						      <h4 class="panel-title search">
						        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
						          Manufacturing and Construction <span class="text-view-all pull-right">View all</span>
						        </a>
						      </h4>
						    </div>
						    <div id="collapseTwo" class="panel-collapse collapse">
						      <div class="panel-body">
						        <div class="category-listing">
					  					<h4 class="categories-heading">Biotechnology 
					  						<span class="text-view-product pull-right">VIEW ALL PRODUCTS IN THIS CATEGORY</span>

					  					</h4> 
					  					<ul class="list-inline category-list">
					  						<li>Genomics</li>
					  						<li>Markets</li>
					  						<li>Technology</li>
					  						<li>Fast Facts</li>
					  						<li>Companies</li>
					  						<li>Proteomics</li>
					  						<li>Bioinformatics</li>
					  					</ul>
					  				</div>
						      </div>
						    </div>
						  </div>
						  <div class="panel panel-default">
						    <div class="panel-heading">
						      <h4 class="panel-title search">
						        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
						         Energy and Transport  <span class="text-view-all pull-right">View all</span>
						        </a>
						      </h4>
						    </div>
						    <div id="collapseThree" class="panel-collapse collapse">
						      <div class="panel-body">
						        <div class="category-listing">
					  					<h4 class="categories-heading">Biotechnology 
					  						<span class="text-view-product pull-right">VIEW ALL PRODUCTS IN THIS CATEGORY</span>

					  					</h4> 
					  					<ul class="list-inline category-list">
					  						<li>Genomics</li>
					  						<li>Markets</li>
					  						<li>Technology</li>
					  						<li>Fast Facts</li>
					  						<li>Companies</li>
					  						<li>Proteomics</li>
					  						<li>Bioinformatics</li>
					  					</ul>
					  				</div>
						      </div>
						    </div>
						  </div>
						  <div class="panel panel-default">
						    <div class="panel-heading">
						      <h4 class="panel-title search">
						        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
						         Telecommunications and Computing  <span class="text-view-all pull-right">View all</span>
						        </a>
						      </h4>
						    </div>
						    <div id="collapseFour" class="panel-collapse collapse">
						      <div class="panel-body">
						       <div class="category-listing">
					  					<h4 class="categories-heading">Biotechnology 
					  						<span class="text-view-product pull-right">VIEW ALL PRODUCTS IN THIS CATEGORY</span>

					  					</h4> 
					  					<ul class="list-inline category-list">
					  						<li>Genomics</li>
					  						<li>Markets</li>
					  						<li>Technology</li>
					  						<li>Fast Facts</li>
					  						<li>Companies</li>
					  						<li>Proteomics</li>
					  						<li>Bioinformatics</li>
					  					</ul>
					  				</div>
						      </div>
						    </div>
						  </div>
						  <div class="panel panel-default">
						    <div class="panel-heading">
						      <h4 class="panel-title search">
						        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseFive">
						         Government and Public Sector    <span class="text-view-all pull-right">View all</span>
						        </a>
						      </h4>
						    </div>
						    <div id="collapseFive" class="panel-collapse collapse">
						      <div class="panel-body">
						        <div class="category-listing">
					  					<h4 class="categories-heading">Biotechnology 
					  						<span class="text-view-product pull-right">VIEW ALL PRODUCTS IN THIS CATEGORY</span>

					  					</h4> 
					  					<ul class="list-inline category-list">
					  						<li>Genomics</li>
					  						<li>Markets</li>
					  						<li>Technology</li>
					  						<li>Fast Facts</li>
					  						<li>Companies</li>
					  						<li>Proteomics</li>
					  						<li>Bioinformatics</li>
					  					</ul>
					  				</div>
						      </div>
						    </div>
						  </div>
						</div>
	  			<!--end here accordion-->
				</div>
				<!--start add section-->
					<div class="adds-section search-page right hidden-laptop hidden-md hidden-xs">
						<div class="adds-img">
							<a href="#"><img src="images/adds-report1.png" alt="add-1" class="img-responsive"> </a>
						</div>
						<div class="adds-img">
							<a href="#"><img src="images/adds-report.png" alt="add-1" class="img-responsive"> </a>
						</div>
					</div>
				<!--end add section -->
		  	</div>

		  	<!--footer online icons-->
		  	<div class="row online-icon-section">
	  			<div class="col-lg-6 col-sm-4 col-xs-6 pull-left ">
	  				<div class="secured-img"><a href="#"><img src="images/secured-img.png" alt="secured-by" class="img-responsive"></a></div>
	  			</div>
	  			<div class="col-lg-6 col-sm-8 col-xs-12">
					<div class="row">
						<div class="col-lg-7 col-md-6 col-sm-7 col-xs-8">
							<p class="credit-card-text">We accept all major credit cards!</p>
							<ul class="list-inline">
								<li><a href="#"><img src="images/icon_1.jpg" alt="secured-by" class="img-responsive"></a></li>
								<li><a href="#"><img src="images/icon_2.jpg" alt="secured-by" class="img-responsive"></a></li>
								<li><a href="#"><img src="images/icon_3.jpg" alt="secured-by" class="img-responsive"></a></li>
								<li><a href="#"><img src="images/icon_4.jpg" alt="secured-by"class="img-responsive"></a></li>
								<li><a href="#"><img src="images/icon_5.jpg" alt="secured-by" class="img-responsive"></a></li>
							</ul>
						</div>
						<div class="col-lg-5 col-md-6 col-sm-5 col-xs-4">
							<p class="credit-card-text">No PayPal account required</p>
							<div class="paypal-img">
								<a href="#"><img src="images/icon_6.jpg" alt="secured-by" class="img-responsive"></a>
							</div>
						</div>
				  	</div>
				</div>
		  	</div>
		  	<!--footer online icons end here -->
	  	</div>
	  	<!--end login here-->
	  	
      <?php include("footer.php"); ?>
      <?php include("script.php"); ?>
	   <script>
			$('.alert-item').imagesLoaded( function() {
				$('.alert-item').masonry({
				  itemSelector: '.alert-box',
				});

				setTimeout(function() {
					$('.alert-item').masonry('layout');
				},500);
			});

		</script>
	  
   </body>
   <!-- Body end -->
</html>