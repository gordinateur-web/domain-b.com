<footer>
	<div class="footer-menu">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<ul class="list-unstyled text-center">
						<li><a href="">home .</a></li>
						<li><a href="">advertise .</a></li>
						<li><a href="">register .</a></li>
						<li><a href="">about TIC .</a></li>
						<li><a href="">contact us .</a></li>
						<li><a href="">site map</a></li>
						<li><a href=""><strong>also visit:</strong> prdomain.com .</a></li>
						<li><a href="http://www.ticworks.com" target="_blank">ticworks.com</a></li>
					</ul>
				</div>
			</div>
		</div>
		<div class="container-fluid footer-black-patch">
			<p class="text-center"><a href="">Legal Policy </a>| Copyright &copy; 1999-2007 The Information Company Private Limited. All rights reserved.</p>
		</div>
	</div>
</footer>