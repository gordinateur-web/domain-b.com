<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title>Product Page | Domain-b </title>
      <?php include("head.php"); ?>
     
   </head>
   <body class="">
      <!-- Body start -->
      <?php include("header1.php"); ?>
     	 <!-- Breadcrumb Section start -->
			<div class="breadcrumb-section">
				<div class="container">
					<ol class="breadcrumb">
					  <li><a href="#">Home</a></li>
					  <li><a href="#">Categories  </a></li>
					  <li><a href="#">Pharmaceuticals and Healthcare    </a></li>
					  <li><a href="#">Biotechnology  </a></li>
					</ol>
				</div>
			</div>
		<!-- Breadcrumb section end -->

      <!--login window start here-->
	  	<div class="container">
	  		<div class="row product-details">
	  			
	  			<!--details start here-->
		  		<div class="col-lg-9 col-md-9 col-sm-8 col-xs-12">
		  			<h3 class="product-name">Biological Identification. Woodhead Publishing Series in Electronic and Optical Materials</h3>
		  				<div class="table-responsive">          
							<table class="table order-confirmation product-page" id="product-page">
								<thead>
									<tr>
								       <td class="col-lg-1 col-md-1 col-sm-1 col-xs-4">Publishing date :</td>
								        <td class="col-lg-2 col-md-2 col-sm-2 col-xs-8">March 2014</td>
									</tr>
								</thead>
								<tbody>
								      <tr>
								       <td>Pages: </td>
								        <td>136</td>
								       </tr>
								       <tr>
								       <td>Price:</td>
								        <td>
								        	<select class="form-control  user-product" required>
							      			<option value="selected">Single user: <strong>US$ 3,950.00</strong></option>
							       			<option value="0">Single user: <strong>US$ 3,950.00</strong></option>
							        		<option value="1">Single user: <strong>US$ 3,950.00</strong></option>
											</select>
										</td>
								       </tr>
								       <tr>
								       <td>Publisher :</td>
								        <td>Global Industry Analysts, Inc</td>
								       </tr>

								       <tr>
								       <td>Report type :</td>
								        <td>Strategic report</td>
								       </tr>
								       <tr>
								       <td>Delivery: </td>
								        <td>E-mail delivery (PDF)</td>
								       </tr>
								 </tbody>
							</table>
						</div>
						<button type="button" class="btn btn-default brown-button product-page">Buy now  <span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span></button>
						<button type="button" class="btn btn-default brown-button product-page"> Add to cart <span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span> </button>
    			</div>
		  		<!--details form end here-->
		  		
		  		<div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
	  				<ul class="list-inline product-page-list">
	  					<li><a href="#"><span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span>Inquiry before buying</a></li>
	  					<li><a href="#"><span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span>Request a sample</a></li>
	  					<li><a href="#"><span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span>Download PDF brochure</a></li>
	  					<li><a href="#"><span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span>Query about this report?</a></li>
	  					<li><a href="#"><span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span>FAQs</a></li>
					</ul>
		  		</div>
		  
		  	</div>

		  	<div class="row">
		  		<div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
			  		<ul class="nav nav-tabs product-page">
					    <li class="active"><a data-toggle="tab" href="#home">Description  </a></li>
					    <li><a data-toggle="tab" href="#menu1">Table of content  </a></li>
					    <li><a data-toggle="tab" href="#menu2">Summary</a></li>
					</ul>
					 <div class="tab-content">
				    	<div id="home" class="tab-pane fade in active">
						<p class="black-text product">
				      			Biological identification systems are a vital component in countering the threats to life and health posed by natural pathogens, toxins, and bioterrorism agents. It is essential that such systems are fast, accurate, reliable and easy to use. It is also important to employ the most suitable technology in dealing with any particular threat. This book provides a detailed review of the technologies available and the potential for future developments.

				      		</p>

				      		<p class="black-text product">
				      			Part one covers the essentials of DNA and RNA sequencing for the identification of pathogens, including next generation sequencing (NGS), polymerase chain reaction (PCR) methods, isothermal amplification and bead array technologies. Part two addresses a variety of approaches to making identification systems portable, tackling the special requirements of smaller, mobile systems in fluid movement, power usage, and sample preparation. Part three focuses in on a range of optical methods and their advantages. Finally, part four describes a unique approach to sample preparation and a promising approach to identification using mass spectroscopy.

				      		</p>
				      		<p class="black-text product">
				      			Biological Identification will be a useful resource for academics and engineers involved in the microelectronics and sensors industry, and for companies, medical organisations and military bodies looking for biodetection solutions.
				      		</p>
				      		<p class="black-text product">
				      		 Covers DNA sequencing of pathogens, lab-on-chip and portable systems for biodetection and analysis
				      		</p>
				      		<p class="black-text product">
								 Provides an in-depth description of optical systems and explores sample preparation and mass spectrometry-based biological analysis
				      		</p>

				    	</div>
					    <div id="menu1" class="tab-pane fade">
					      <p class="black-text product">
				      			Part one covers the essentials of DNA and RNA sequencing for the identification of pathogens, including next generation sequencing (NGS), polymerase chain reaction (PCR) methods, isothermal amplification and bead array technologies. Part two addresses a variety of approaches to making identification systems portable, tackling the special requirements of smaller, mobile systems in fluid movement, power usage, and sample preparation. Part three focuses in on a range of optical methods and their advantages. Finally, part four describes a unique approach to sample preparation and a promising approach to identification using mass spectroscopy.

				      		</p>
					    </div>
					    <div id="menu2" class="tab-pane fade">
					      <p class="black-text product">
				      			Part one covers the essentials of DNA and RNA sequencing for the identification of pathogens, including next generation sequencing (NGS), polymerase chain reaction (PCR) methods, isothermal amplification and bead array technologies. Part two addresses a variety of approaches to making identification systems portable, tackling the special requirements of smaller, mobile systems in fluid movement, power usage, and sample preparation. Part three focuses in on a range of optical methods and their advantages. Finally, part four describes a unique approach to sample preparation and a promising approach to identification using mass spectroscopy.

				      		</p>
					    </div>
					 </div>
					</div>
			  	
			  	<div class="col-lg-2 col-md-2 hidden-sm hidden-xs">
			  		<div class="product-adds"><a href="#"><img src="images/adds-product.jpg" alt="product-img" class="img-responsive"></a></div>	
			  	</div>

			  	<!--order summary section start here-->
		  		<div class="col-lg-3 col-md-5 col-sm-5 col-xs-12">
		  			<section class="related-post">
		  				<div class="post-details-title"><h3>Related reports</h3></div>
							<div class="post-summary">
		  						<p class="summary-details-text">
		  							Biological Identification. Woodhead Publishing Series in Electronic and Optical Materials
								</p>
		  						<p class="summary-user"><strong>USD 230  </strong>| Apr-14  </p>
		  					</div>
		  					<div class="post-summary">
		  						<p class="summary-details-text">Research and Development in Biotechnology Industry (U.S.): Analytics, Extensive Financial Benchmarks, Metrics and Revenue Forecasts to 2020, NAIC 541711<br/>
		  						A Global Market Study</p>
		  						<p class="summary-user"><strong>USD 3,077 </strong> | Apr-14  </p>
		  					</div>
		  					<div class="post-summary">
		  						<p class="summary-details-text">Research and Development in Biotechnology Industry (U.S.): Analytics, Extensive Financial Benchmarks, Metrics and Revenue Forecasts to 2020, NAIC 541711<br/>
		  						A Global Market Study</p>
		  						<p class="summary-user"><strong>USD 1,995 </strong> | Apr-14  </p>
		  					</div>
		  					<div class="post-summary">
		  						<p class="summary-details-text">Biotechnology for the Non Biotechnologist (Course, France)<br/>
		  						A Global Market Study</p>
		  						<p class="summary-user"><strong>USD 129 </strong> | Apr-14  </p>
		  					</div>
		  					<div class="post-summary">
		  						<p class="summary-details-text">Biological Identification. Woodhead Publishing Series in Electronic and Optical Materials</p>
		  						<p class="summary-user"><strong>USD 2,150 </strong> | Apr-14  </p>
		  					</div>
		  					<div class="post-summary">
		  						<p class="summary-details-text">Biotechnology for the Non Biotechnologist (Course, France)
		  						<p class="summary-user"><strong>USD 3,460 </strong> | Apr-14  </p>
		  					</div>
		  				</section>
		  			</div>
		  			
				</div>
		  		<!--footer online icons-->
		  <div class="row online-icon-section">
	  			<div class="col-lg-6 col-sm-4 col-xs-6 pull-left ">
	  				<div class="secured-img"><a href="#"><img src="images/secured-img.png" alt="secured-by" class="img-responsive"></a></div>
	  			</div>
	  			<div class="col-lg-6 col-sm-8 col-xs-12">
					<div class="row">
						<div class="col-lg-7 col-md-6 col-sm-7 col-xs-8">
							<p class="credit-card-text">We accept all major credit cards!</p>
							<ul class="list-inline">
								<li><a href="#"><img src="images/icon_1.jpg" alt="secured-by" class="img-responsive"></a></li>
								<li><a href="#"><img src="images/icon_2.jpg" alt="secured-by" class="img-responsive"></a></li>
								<li><a href="#"><img src="images/icon_3.jpg" alt="secured-by" class="img-responsive"></a></li>
								<li><a href="#"><img src="images/icon_4.jpg" alt="secured-by"class="img-responsive"></a></li>
								<li><a href="#"><img src="images/icon_5.jpg" alt="secured-by" class="img-responsive"></a></li>
							</ul>
						</div>
						<div class="col-lg-5 col-md-6 col-sm-5 col-xs-4">
							<p class="credit-card-text">No PayPal account required</p>
							<div class="paypal-img">
								<a href="#"><img src="images/icon_6.jpg" alt="secured-by" class="img-responsive"></a>
							</div>
						</div>
				  	</div>
				</div>
		  	</div>
		  	<!--footer online icons end here -->
			</div>

		  	
	  	</div>
	  	<!--end login here-->
	  	
      <?php include("footer.php"); ?>
      <?php include("script.php"); ?>
	   <script>
			$('.alert-item').imagesLoaded( function() {
				$('.alert-item').masonry({
				  itemSelector: '.alert-box',
				});

				setTimeout(function() {
					$('.alert-item').masonry('layout');
				},500);
			});

		</script>
	  
   </body>
   <!-- Body end -->
</html>