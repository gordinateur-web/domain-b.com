<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title>Order Confirmation | Domain-b </title>
      <?php include("head.php"); ?>
     
   </head>
   <body class="">
      <!-- Body start -->
      <?php include("header1.php"); ?>
     	 <!-- Breadcrumb Section start -->
			<div class="breadcrumb-section">
				<div class="container">
					<ol class="breadcrumb">
					  <li><a href="#">Home</a></li>
					  <li><a href="#">Login/register</a></li>
					</ol>
				</div>
			</div>

		<!-- Breadcrumb section end -->

      <!--login window start here-->
	  	<div class="container">
	  		<div class="row">
				<ul class="nav nav-tabs login-profile">
				    <li class="active"><a data-toggle="tab" href="#home">Your profile details</a></li>
				    <li><a data-toggle="tab" href="#menu1">Edit profile</a></li>
				    <li><a data-toggle="tab" href="#menu2">Change password</a></li>
				    <li><a data-toggle="tab" href="#menu3">Order history</a></li>
				    <li><a data-toggle="tab" href="#menu4">Logout</a></li>
				  </ul>

				  <div class="tab-content">
				    <div id="home" class="tab-pane fade in active">
					    	<div class="row">
					    		<h2 class="login-maintitle">Your profile details</h2>
					    		<div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
						    	 <div class="table-responsive">          
									  	<table class="table order-confirmation">
									    <tbody>
									      <tr>
									       <td><strong>Name: </strong></td>
									        <td>Lokesh Thakur</td>
									       </tr>
									       <tr>
									       <td><strong>Job title : </strong></td>
									        <td>Mng</td>
									       </tr>
									       <tr>
									       <td><strong>Organization:</strong></td>
									        <td>TIC</td>
									       </tr>
									       <tr>
									       <td><strong>Email:</strong></td>
									        <td>lokesh.thakur@ticworks.com</td>
									       </tr>

									       <tr>
									       <td><strong>Contact number:</strong></td>
									        <td>+91 6989431855</td>
									       </tr>
									       <tr>
									       	<tr>
									       <td><strong>Address::</strong></td>
									        <td>6598 Afasdf asdf asdf</td>
									       </tr>
									       <td><strong>City: </strong></td>
									        <td>Navi mumbai</td>
									       </tr>
									       <tr>
									       <td><strong>Country:</strong></td>
									        <td>India</td>
									       </tr>
									       
									    </tbody>
									  </table>
								</div>
						</div>
				    </div>
				    <div id="menu1" class="tab-pane fade">
				      <h3>Menu 1</h3>
				      <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
				    </div>
				    <div id="menu2" class="tab-pane fade">
				    	
				    </div>
				    <div id="menu3" class="tab-pane fade">
				      <h3>Menu 3</h3>
				      <p>Eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
				    </div>
				  </div>
	  			</div>
	  		</div>
			<!--footer online icons-->
		  	<div class="row online-icon-section">
	  			<div class="col-lg-6 col-sm-4 col-xs-6 pull-left ">
	  				<div class="secured-img"><img src="images/secured-img.png" alt="secured-by"></div>
	  			</div>
	  			<div class="col-lg-6 col-sm-8 col-xs-12">
					<div class="row">
						<div class="col-lg-7 col-md-6 col-sm-7 col-xs-8">
							<p class="credit-card-text">We accept all major credit cards!</p>
							<ul class="list-inline">
								<li><img src="images/icon_1.jpg" alt="secured-by"></li>
								<li><img src="images/icon_2.jpg" alt="secured-by"></li>
								<li><img src="images/icon_3.jpg" alt="secured-by"></li>
								<li><img src="images/icon_4.jpg" alt="secured-by"></li>
								<li><img src="images/icon_5.jpg" alt="secured-by"></li>
							</ul>
						</div>
						<div class="col-lg-5 col-md-6 col-sm-5 col-xs-4">
							<p class="credit-card-text">No PayPal account required</p>
							<div class="paypal-img">
								<img src="images/icon_6.jpg" alt="secured-by">
							</div>
						</div>
				  	</div>
				</div>
		  	</div>
		  	<!--footer online icons end here -->
	  	</div>
	  	<!--end login here-->
	  	
      <?php include("footer.php"); ?>
      <?php include("script.php"); ?>
	   <script>
			$('.alert-item').imagesLoaded( function() {
				$('.alert-item').masonry({
				  itemSelector: '.alert-box',
				});

				setTimeout(function() {
					$('.alert-item').masonry('layout');
				},500);
			});

		</script>
	  
   </body>
   <!-- Body end -->
</html>