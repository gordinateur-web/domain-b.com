<header>
 <div class="black-patch">	
   <div class="container">
      <div class="row pre-header">
         <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
		   <!-- pre-header Lhs ul start -->
            <ul class="list-inline list-unstyled left-nav-pre-header">
               <li class="hidden-xs"><a href="#">subscribe to newsletter</a></li>
               <li class="hidden-xs"><a href="#">feedback</a></li>
               <li class="hidden-xs"><a href="#">Advertise</a></li>
               <li><a href="#">register</a></li>
               <li class="hidden-xs"><a href="#">about TIC</a></li>
               <li class="hidden-xs"><a href="#">contact us</a></li>
               <li><a href="#">Sign in</a></li>
            </ul>
            <!-- pre-header Lhs ul end -->
         </div>
			<!-- This section is hidden in mobile -->
         <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 hidden-xs">
			<!-- pre-header Rhs ul start -->
            <ul class="list-inline list-unstyled pull-right right-nav-pre-header">
               <li><a href="#"><img src="images/rss-ico.png" class="img-responsive rss-ico" border="0" alt=""></a></li>
               <li><a href="#"><img src="images/linkedin-ico.png" class="img-responsive linkedin-ico" border="0" alt=""></a></li>
               <li><a href="#"><img src="images/twitter-follow-ico.png" class="img-responsive twitter-follow-ico" border="0" alt=""></a></li>
               <li><a href="#"><img src="images/fb-like.png" class="img-responsive fb-like" border="0" alt=""></a></li>
            </ul>
            <!-- pre-header Rhs ul end -->
         </div>
      </div>
   </div>
   </div>
    <!-- logo container start -->
   <div class="logo-container">
      <div class="container">
         <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
               <div class="logo">
                  <a href="#"><img src="images/domain-b-logo.png" class="img-responsive" border="0" alt="domain-b logo" title="domain-b logo"></a>
               </div>
            </div>
            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
               <div class="header-advertisement horizontal-advertisement">
                  <img src="images/header-adv.jpg" class="img-responsive" border="0" alt="">
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- logo container end -->
    <!-- navbar container start -->
   <div class="navbar-container">
      <div class="container">
         <div class="row">
            <div class="navbar" role="navigation">
               <div class="navbar-header">
				 <!-- only visible in mobile -->
                  <div class="pull-left">
                     <button class="visible-xs special-section-btn" id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                     Special sections
                     <span class="caret"></span>
                     </button>
                     <div class="visible-xs toggle-search-box">
                      <span class="glyphicon glyphicon-search search-icon"></span>
						      <i class="fa fa-times" aria-hidden="true"></i> 
                     </div>
                  </div>
                  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                  <span class="sr-only">Toggle navigation</span>
                  <span class="icon-bar one"></span>
                  <span class="icon-bar two"></span>
                  <span class="icon-bar three"></span>
                  </button>
               </div>
             
			   <div class="collapse navbar-collapse">
                  <ul class="nav navbar-nav">
                     <li class="dropdown show-on-hover">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">PEOPLE <span class="pull-right visible-xs">
                        </span></a>
					 <!-- people menu dropdown start -->
                        <ul class="dropdown-menu ie-dropdown">
                           <li><a href="#">Action [Menu 1.1]</a></li>
                           <li><a href="#">Another action [Menu 1.1]</a></li>
                           <li><a href="#">Something else here [Menu 1.1]</a></li>
                           <li><a href="#">Separated link [Menu 1.1]</a></li>
                           <li><a href="#">One more separated link [Menu 1.1]</a></li>
                           <li class="dropdown-submenu">
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown [Menu 1.1] <span class="pull-right">
                              <i class="glyphicon glyphicon-menu-right hidden-xs" aria-hidden="true"></i>
                              </span></a>
                              <ul class="dropdown-menu">
                                 <li><a href="#">Action [Menu 1.2]</a></li>
                                 <li>
                                    <a href="#">Dropdown [Menu 1.2] </a>
                                 </li>
                              </ul>
                           </li>
                        </ul>
                        <!-- people menu dropdown end -->
                     </li>
                     <li class="dropdown show-on-hover">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">COMPANIES/ORG <span class="pull-right visible-xs">
                        </span></a>
					<!-- COMPANIES/ORG menu dropdown start -->
                        <ul class="dropdown-menu ie-dropdown">
                           <li><a href="#">Action [Menu 1.1]</a></li>
                           <li><a href="#">Another action [Menu 1.1]</a></li>
                           <li><a href="#">Something else here [Menu 1.1]</a></li>
                           <li><a href="#">Separated link [Menu 1.1]</a></li>
                           <li><a href="#">One more separated link [Menu 1.1]</a></li>
                           <li class="dropdown-submenu">
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown [Menu 1.1] <span class="pull-right">
                              <i class="glyphicon glyphicon-menu-right hidden-xs" aria-hidden="true"></i>
                              </span></a>
                              <ul class="dropdown-menu">
                                 <li><a href="#">Action [Menu 1.2]</a></li>
                                 <li>
                                    <a href="#">Dropdown [Menu 1.2] </a>
                                 </li>
                              </ul>
                           </li>
                        </ul>
                        <!-- COMPANIES/ORG menu dropdown end -->
                     </li>
                     <li class="dropdown show-on-hover">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">MANAGEMENT <span class="pull-right visible-xs">
                        </span></a>
					 <!-- MANAGEMENT menu dropdown start -->
                        <ul class="dropdown-menu ie-dropdown">
                           <li><a href="#">Action [Menu 1.1]</a></li>
                           <li><a href="#">Another action [Menu 1.1]</a></li>
                           <li><a href="#">Something else here [Menu 1.1]</a></li>
                           <li><a href="#">Separated link [Menu 1.1]</a></li>
                           <li><a href="#">One more separated link [Menu 1.1]</a></li>
                           <li class="dropdown-submenu">
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown [Menu 1.1] <span class="pull-right">
                              <i class="glyphicon glyphicon-menu-right hidden-xs" aria-hidden="true"></i>
                              </span></a>
                              <ul class="dropdown-menu">
                                 <li><a href="#">Action [Menu 1.2]</a></li>
                                 <li>
                                    <a href="#">Dropdown [Menu 1.2] </a>
                                 </li>
                              </ul>
                           </li>
                        </ul>
                        <!-- MANAGEMENT menu dropdown end -->
                     </li>
                     <li class="dropdown show-on-hover">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">INDUSTRY <span class="pull-right visible-xs">
                        </span></a>
						<!-- INDUSTRY menu dropdown start -->
                        <ul class="dropdown-menu ie-dropdown">
                           <li><a href="#">Action [Menu 1.1]</a></li>
                           <li><a href="#">Another action [Menu 1.1]</a></li>
                           <li><a href="#">Something else here [Menu 1.1]</a></li>
                           <li><a href="#">Separated link [Menu 1.1]</a></li>
                           <li><a href="#">One more separated link [Menu 1.1]</a></li>
                           <li class="dropdown-submenu">
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown [Menu 1.1] <span class="pull-right">
                              <i class="glyphicon glyphicon-menu-right hidden-xs" aria-hidden="true"></i>
                              </span></a>
                              <ul class="dropdown-menu">
                                 <li><a href="#">Action [Menu 1.2]</a></li>
                                 <li>
                                    <a href="#">Dropdown [Menu 1.2] </a>
                                 </li>
                              </ul>
                           </li>
                        </ul>
                        <!-- INDUSTRY dropdown menu end -->
                     </li>
                     <li class="dropdown show-on-hover">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">TECHNOLOGY <span class="pull-right visible-xs">
                        </span></a>
					<!-- TECHNOLOGY menu dropdown start -->
                        <ul class="dropdown-menu ie-dropdown">
                           <li><a href="#">Action [Menu 1.1]</a></li>
                           <li><a href="#">Another action [Menu 1.1]</a></li>
                           <li><a href="#">Something else here [Menu 1.1]</a></li>
                           <li><a href="#">Separated link [Menu 1.1]</a></li>
                           <li><a href="#">One more separated link [Menu 1.1]</a></li>
                           <li class="dropdown-submenu">
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown [Menu 1.1] <span class="pull-right">
                              <i class="glyphicon glyphicon-menu-right hidden-xs" aria-hidden="true"></i>
                              </span></a>
                              <ul class="dropdown-menu">
                                 <li><a href="#">Action [Menu 1.2]</a></li>
                                 <li>
                                    <a href="#">Dropdown [Menu 1.2] </a>
                                 </li>
                              </ul>
                           </li>
                        </ul>
                        <!-- TECHNOLOGY menu dropdown end -->
                     </li>
                     <li class="dropdown show-on-hover">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">FINANCE <span class="pull-right visible-xs">
                        </span></a>
					<!-- FINANCE menu dropdown start -->
                        <ul class="dropdown-menu ie-dropdown">
                           <li><a href="#">Action [Menu 1.1]</a></li>
                           <li><a href="#">Another action [Menu 1.1]</a></li>
                           <li><a href="#">Something else here [Menu 1.1]</a></li>
                           <li><a href="#">Separated link [Menu 1.1]</a></li>
                           <li><a href="#">One more separated link [Menu 1.1]</a></li>
                           <li class="dropdown-submenu">
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown [Menu 1.1] <span class="pull-right">
                              <i class="glyphicon glyphicon-menu-right hidden-xs" aria-hidden="true"></i>
                              </span></a>
                              <ul class="dropdown-menu">
                                 <li><a href="#">Action [Menu 1.2]</a></li>
                                 <li>
                                    <a href="#">Dropdown [Menu 1.2] </a>
                                 </li>
                              </ul>
                           </li>
                        </ul>
                        <!-- FINANCE menu dropdown end -->
                     </li>
                     <li class="dropdown show-on-hover fourth-last-drop-sm">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">MARKETS <span class="pull-right visible-xs">
                        </span></a>
					 <!-- MARKETS menu dropdown start -->
                       <ul class="dropdown-menu ie-dropdown">
                           <li><a href="#">Action [Menu 1.1]</a></li>
                           <li><a href="#">Another action [Menu 1.1]</a></li>
                           <li><a href="#">Something else here [Menu 1.1]</a></li>
                           <li><a href="#">Separated link [Menu 1.1]</a></li>
                           <li><a href="#">One more separated link [Menu 1.1]</a></li>
                           <li class="dropdown-submenu">
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown [Menu 1.1] <span class="pull-right">
                              <i class="glyphicon glyphicon-menu-right hidden-xs" aria-hidden="true"></i>
                              </span></a>
                              <ul class="dropdown-menu">
                                 <li><a href="#">Action [Menu 1.2]</a></li>
                                 <li>
                                    <a href="#">Dropdown [Menu 1.2] </a>
                                 </li>
                              </ul>
                           </li>
                        </ul>
                        <!-- MARKETS menu dropdown end -->
                     </li>
                     <li class="dropdown show-on-hover third-last-drop-sm">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">ECONOMY <span class="pull-right visible-xs">
                        </span></a>
						<!-- ECONOMY menu dropdown start -->
                        <ul class="dropdown-menu ie-dropdown">
                           <li><a href="#">Action [Menu 1.1]</a></li>
                           <li><a href="#">Another action [Menu 1.1]</a></li>
                           <li><a href="#">Something else here [Menu 1.1]</a></li>
                           <li><a href="#">Separated link [Menu 1.1]</a></li>
                           <li><a href="#">One more separated link [Menu 1.1]</a></li>
                           <li class="dropdown-submenu">
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown [Menu 1.1] <span class="pull-right">
                              <i class="glyphicon glyphicon-menu-right hidden-xs" aria-hidden="true"></i>
                              </span></a>
                              <ul class="dropdown-menu">
                                 <li><a href="#">Action [Menu 1.2]</a></li>
                                 <li>
                                    <a href="#">Dropdown [Menu 1.2] </a>
                                 </li>
                              </ul>
                           </li>
                        </ul>
                        <!-- ECONOMY menu dropdown end -->
                     </li>
                     <li class="dropdown show-on-hover sec-last-drop-sm">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">ENVIRONMENT <span class="pull-right visible-xs">
                        </span></a>
					<!-- ENVIRONMENT menu dropdown start -->
                        <ul class="dropdown-menu ie-dropdown">
                           <li><a href="#">Action [Menu 1.1]</a></li>
                           <li><a href="#">Another action [Menu 1.1]</a></li>
                           <li><a href="#">Something else here [Menu 1.1]</a></li>
                           <li><a href="#">Separated link [Menu 1.1]</a></li>
                           <li><a href="#">One more separated link [Menu 1.1]</a></li>
                           <li class="dropdown-submenu">
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown [Menu 1.1] <span class="pull-right">
                              <i class="glyphicon glyphicon-menu-right hidden-xs" aria-hidden="true"></i>
                              </span></a>
                              <ul class="dropdown-menu">
                                 <li><a href="#">Action [Menu 1.2]</a></li>
                                 <li>
                                    <a href="#">Dropdown [Menu 1.2] </a>
                                 </li>
                              </ul>
                           </li>
                        </ul>
                        <!-- ENVIRONMENT menu dropdown end -->
                     </li>
                     <li class="dropdown show-on-hover last-drop">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">DEFENCE <span class="pull-right visible-xs">
                        </span></a>
					<!-- DEFENCE menu dropdown start -->
                        <ul class="dropdown-menu ie-dropdown">
                           <li><a href="#">Action [Menu 1.1]</a></li>
                           <li><a href="#">Another action [Menu 1.1]</a></li>
                           <li><a href="#">Something else here [Menu 1.1]</a></li>
                           <li><a href="#">Separated link [Menu 1.1]</a></li>
                           <li><a href="#">One more separated link [Menu 1.1]</a></li>
                           <li class="dropdown-submenu">
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown [Menu 1.1]<span class="pull-right">
                              <i class="glyphicon glyphicon-menu-right hidden-xs" aria-hidden="true"></i>
                              </span></a>
                              <ul class="dropdown-menu">
                                 <li><a href="#">Action [Menu 1.2]</a></li>
                                 <li>
                                    <a href="#">Dropdown [Menu 1.2] </a>
                                 </li>
                              </ul>
                           </li>
                        </ul>
                        <!-- DEFENCE menu dropdown end -->
                     </li>
                  </ul>
               </div> 
               <!--/.nav-collapse -->
            </div>
         </div>
      </div>
   </div>
   <!--navbar container end -->
    <!-- special-section-container start -->
   <div class="special-section-container">
      <div class="container">
         <div class="row">
            <ul class="list-inline mobile-list-inline list-unstyled special-section-toggle"  aria-labelledby="dLabel">
               <span class="text-red text-uppercase hidden-xs">Special sections:</span>
              <!-- <li class="dropdown show-on-hover">
				  <a href="#" class="dropdown-toggle" data-toggle="dropdown">aerospace <span class="pull-right visible-xs"></span></a>
                        <ul class="dropdown-menu">
                           <li><a href="#">Action [Menu 1.1]</a></li>
                           <li><a href="#">Another action [Menu 1.1]</a></li>
                           <li><a href="#">Something else here [Menu 1.1]</a></li>
                           <li><a href="#">Separated link [Menu 1.1]</a></li>
                           <li><a href="#">One more separated link [Menu 1.1]</a></li>
                           <li><a href="#">Something else here [Menu 1.1]</a></li>
                        </ul>       
				</li> -->
               <li><a href="#">alerts</a></li>
               <li><a href="#">auto zone</a></li>
               <li><a href="#">brand dossier</a></li>
               <li><a href="#">financials</a></li>
               <li><a href="#">goodlife</a></li>
               <li><a href="#">infotech</a></li>
               <li><a href="#">videos</a></li>
               <li class="pull-right col-lg-3 col-md-3 col-sm-3 hidden-xs">
                  <div class="search-box-header">
                     <div class="input-group stylish-input-group">
                        <input type="text" class="form-control holder-color"  placeholder="Search" >
                        <span class="input-group-addon">
                        <button type="submit">
                        <span class="glyphicon glyphicon-search"></span>
                        </button>  
                        </span>
                     </div>
                  </div>
               </li>
            </ul>
         </div>
	 <!-- this section visible in mobile -->
         <div class="search-box-header-mobile hidden-lg hidden-md hidden-sm">
            <div class="input-group stylish-input-group">
               <input type="text" class="form-control holder-color"  placeholder="Search" >
               <span class="input-group-addon">
               <button type="submit">
               <span class="glyphicon glyphicon-search"></span>
               </button>  
               </span>
            </div>
         </div>
      </div>
   </div>
   <!-- special-section-container end -->
</header>