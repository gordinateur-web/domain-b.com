<header>
 <div class="black-patch"> 
   <div class="container">
      <div class="row pre-header">
         <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
         <!-- pre-header Lhs ul start -->
            <ul class="list-inline list-unstyled left-nav-pre-header">
               <li class="hidden-xs"><a href="#">subscribe to newsletter</a></li>
               <li class="hidden-xs"><a href="#">feedback</a></li>
               <li class="hidden-xs"><a href="#">Advertise</a></li>
               <li><a href="#">register</a></li>
               <li class="hidden-xs"><a href="#">about TIC</a></li>
               <li class="hidden-xs"><a href="#">contact us</a></li>
               <li><a href="#">Sign in</a></li>
            </ul>
            <!-- pre-header Lhs ul end -->
         </div>
         <!-- This section is hidden in mobile -->
         <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 hidden-xs">
         <!-- pre-header Rhs ul start -->
            <ul class="list-inline list-unstyled pull-right right-nav-pre-header">
               <li><a href="#"><img src="images/rss-ico.png" class="img-responsive rss-ico" border="0" alt=""></a></li>
               <li><a href="#"><img src="images/linkedin-ico.png" class="img-responsive linkedin-ico" border="0" alt=""></a></li>
               <li><a href="#"><img src="images/twitter-follow-ico.png" class="img-responsive twitter-follow-ico" border="0" alt=""></a></li>
               <li><a href="#"><img src="images/fb-like.png" class="img-responsive fb-like" border="0" alt=""></a></li>
            </ul>
            <!-- pre-header Rhs ul end -->
         </div>
      </div>
   </div>
   </div>
    <!-- logo container start -->
   <div class="logo-container">
      <div class="container">
         <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
               <div class="logo">
                  <a href="#"><img src="images/domain-b-logo.png" class="img-responsive" border="0" alt="domain-b logo" title="domain-b logo"></a>
               </div>
            </div>
            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
               <div class="header-advertisement horizontal-advertisement">
                  <img src="images/header-adv.jpg" class="img-responsive" border="0" alt="">
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- logo container end -->
    <!-- navbar container start -->
   <div class="navbar-container border-bottom add-background">
      <div class="container">
         <div class="row">
            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                  <div class="navbar" role="navigation">
                     <div class="navbar-header">
                   <!-- only visible in mobile -->
                        <div class="pull-left ">
                           <ul class="checkout-listing visible-xs">
                              <li>
                                 <a href="cart.php"><img src="images/cart.png" alt="cart img" class="img-responsive cart-img"></a>
                              </li>
                              <li><a href="checkout.php"><span>Checkout</span></a></li>
                           </ul>
                        </div>
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar one"></span>
                        <span class="icon-bar two"></span>
                        <span class="icon-bar three"></span>
                        </button>
                     </div>
                   
                  <div class="collapse navbar-collapse">
                        <ul class="nav navbar-nav">
                           <li class="dropdown show-on-hover">
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown">HOME <span class="pull-right visible-xs">
                              </span></a>
                      <!-- people menu dropdown start -->
                              <ul class="dropdown-menu ie-dropdown">
                                 <li><a href="#">Action [Menu 1.1]</a></li>
                                 <li><a href="#">Another action [Menu 1.1]</a></li>
                                 <li><a href="#">Something else here [Menu 1.1]</a></li>
                                 <li><a href="#">Separated link [Menu 1.1]</a></li>
                                 <li><a href="#">One more separated link [Menu 1.1]</a></li>
                                 <li class="dropdown-submenu">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown [Menu 1.1] <span class="pull-right">
                                    <i class="glyphicon glyphicon-menu-right hidden-xs" aria-hidden="true"></i>
                                    </span></a>
                                    <ul class="dropdown-menu">
                                       <li><a href="#">Action [Menu 1.2]</a></li>
                                       <li>
                                          <a href="#">Dropdown [Menu 1.2] </a>
                                       </li>
                                    </ul>
                                 </li>
                              </ul>
                              <!-- people menu dropdown end -->
                           </li>
                           <li class="dropdown show-on-hover">
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown">CATEGORIES <span class="pull-right visible-xs">
                              </span></a>
                     <!-- COMPANIES/ORG menu dropdown start -->
                              <ul class="dropdown-menu ie-dropdown">
                                 <li><a href="#">Action [Menu 1.1]</a></li>
                                 <li><a href="#">Another action [Menu 1.1]</a></li>
                                 <li><a href="#">Something else here [Menu 1.1]</a></li>
                                 <li><a href="#">Separated link [Menu 1.1]</a></li>
                                 <li><a href="#">One more separated link [Menu 1.1]</a></li>
                                 <li class="dropdown-submenu">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown [Menu 1.1] <span class="pull-right">
                                    <i class="glyphicon glyphicon-menu-right hidden-xs" aria-hidden="true"></i>
                                    </span></a>
                                    <ul class="dropdown-menu">
                                       <li><a href="#">Action [Menu 1.2]</a></li>
                                       <li>
                                          <a href="#">Dropdown [Menu 1.2] </a>
                                       </li>
                                    </ul>
                                 </li>
                              </ul>
                              <!-- COMPANIES/ORG menu dropdown end -->
                           </li>
                           <li class="dropdown show-on-hover">
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown">ABOUT US<span class="pull-right visible-xs">
                              </span></a>
                      <!-- MANAGEMENT menu dropdown start -->
                              <ul class="dropdown-menu ie-dropdown">
                                 <li><a href="#">Action [Menu 1.1]</a></li>
                                 <li><a href="#">Another action [Menu 1.1]</a></li>
                                 <li><a href="#">Something else here [Menu 1.1]</a></li>
                                 <li><a href="#">Separated link [Menu 1.1]</a></li>
                                 <li><a href="#">One more separated link [Menu 1.1]</a></li>
                                 <li class="dropdown-submenu">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown [Menu 1.1] <span class="pull-right">
                                    <i class="glyphicon glyphicon-menu-right hidden-xs" aria-hidden="true"></i>
                                    </span></a>
                                    <ul class="dropdown-menu">
                                       <li><a href="#">Action [Menu 1.2]</a></li>
                                       <li>
                                          <a href="#">Dropdown [Menu 1.2] </a>
                                       </li>
                                    </ul>
                                 </li>
                              </ul>
                              <!-- MANAGEMENT menu dropdown end -->
                           </li>
                           <li class="dropdown show-on-hover">
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown">CONTACT US <span class="pull-right visible-xs">
                              </span></a>
                        <!-- INDUSTRY menu dropdown start -->
                              <ul class="dropdown-menu ie-dropdown">
                                 <li><a href="#">Action [Menu 1.1]</a></li>
                                 <li><a href="#">Another action [Menu 1.1]</a></li>
                                 <li><a href="#">Something else here [Menu 1.1]</a></li>
                                 <li><a href="#">Separated link [Menu 1.1]</a></li>
                                 <li><a href="#">One more separated link [Menu 1.1]</a></li>
                                 <li class="dropdown-submenu">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown [Menu 1.1] <span class="pull-right">
                                    <i class="glyphicon glyphicon-menu-right hidden-xs" aria-hidden="true"></i>
                                    </span></a>
                                    <ul class="dropdown-menu">
                                       <li><a href="#">Action [Menu 1.2]</a></li>
                                       <li>
                                          <a href="#">Dropdown [Menu 1.2] </a>
                                       </li>
                                    </ul>
                                 </li>
                              </ul>
                              <!-- INDUSTRY dropdown menu end -->
                           </li>
                           <li class="dropdown show-on-hover">
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown">DOMAIN-B.COM <span class="pull-right visible-xs">
                              </span></a>
                     <!-- TECHNOLOGY menu dropdown start -->
                              <ul class="dropdown-menu ie-dropdown">
                                 <li><a href="#">Action [Menu 1.1]</a></li>
                                 <li><a href="#">Another action [Menu 1.1]</a></li>
                                 <li><a href="#">Something else here [Menu 1.1]</a></li>
                                 <li><a href="#">Separated link [Menu 1.1]</a></li>
                                 <li><a href="#">One more separated link [Menu 1.1]</a></li>
                                 <li class="dropdown-submenu">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown [Menu 1.1] <span class="pull-right">
                                    <i class="glyphicon glyphicon-menu-right hidden-xs" aria-hidden="true"></i>
                                    </span></a>
                                    <ul class="dropdown-menu">
                                       <li><a href="#">Action [Menu 1.2]</a></li>
                                       <li>
                                          <a href="#">Dropdown [Menu 1.2] </a>
                                       </li>
                                    </ul>
                                 </li>
                              </ul>
                              <!-- TECHNOLOGY menu dropdown end -->
                           </li>
                           <li class="dropdown show-on-hover">
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown">ABOUT TIC <span class="pull-right visible-xs">
                              </span></a>
                     <!-- FINANCE menu dropdown start -->
                              <ul class="dropdown-menu ie-dropdown">
                                 <li><a href="#">Action [Menu 1.1]</a></li>
                                 <li><a href="#">Another action [Menu 1.1]</a></li>
                                 <li><a href="#">Something else here [Menu 1.1]</a></li>
                                 <li><a href="#">Separated link [Menu 1.1]</a></li>
                                 <li><a href="#">One more separated link [Menu 1.1]</a></li>
                                 <li class="dropdown-submenu">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown [Menu 1.1] <span class="pull-right">
                                    <i class="glyphicon glyphicon-menu-right hidden-xs" aria-hidden="true"></i>
                                    </span></a>
                                    <ul class="dropdown-menu">
                                       <li><a href="#">Action [Menu 1.2]</a></li>
                                       <li>
                                          <a href="#">Dropdown [Menu 1.2] </a>
                                       </li>
                                    </ul>
                                 </li>
                              </ul>
                              <!-- FINANCE menu dropdown end -->
                           </li>
                           
                           <li class="dropdown show-on-hover last-drop">
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown">FAQ'S <span class="pull-right visible-xs">
                              </span></a>
                     <!-- DEFENCE menu dropdown start -->
                              <ul class="dropdown-menu ie-dropdown">
                                 <li><a href="#">Action [Menu 1.1]</a></li>
                                 <li><a href="#">Another action [Menu 1.1]</a></li>
                                 <li><a href="#">Something else here [Menu 1.1]</a></li>
                                 <li><a href="#">Separated link [Menu 1.1]</a></li>
                                 <li><a href="#">One more separated link [Menu 1.1]</a></li>
                                 <li class="dropdown-submenu">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown [Menu 1.1]<span class="pull-right">
                                    <i class="glyphicon glyphicon-menu-right hidden-xs" aria-hidden="true"></i>
                                    </span></a>
                                    <ul class="dropdown-menu">
                                       <li><a href="#">Action [Menu 1.2]</a></li>
                                       <li>
                                          <a href="#">Dropdown [Menu 1.2] </a>
                                       </li>
                                    </ul>
                                 </li>
                              </ul>
                              <!-- DEFENCE menu dropdown end -->
                           </li>
                        </ul>
                     </div> 
                     <!--/.nav-collapse -->
                  </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 text-right hidden-xs">
               <ul class="checkout-listing">
                  <li><a href="cart.php">
                     <img src="images/cart.png" alt="cart img" class="img-responsive cart-img">
                     <span>Cart &nbsp;</span><span class="cart-value">0.00 &nbsp; &#36;</span></a>
                  </li>
                  <li><a href="checkout.php"><span>Checkout</span></a></li>
               </ul>
            </div>
         </div>
      </div>
   </div>
   <!--navbar container end -->
    <!-- special-section-container start -->
  
   <!-- special-section-container end -->
</header>