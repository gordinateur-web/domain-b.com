<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/bootstrap-custom.css" rel="stylesheet">
<link href="css/site-layout.css" rel="stylesheet">
<link href="css/media.css" rel="stylesheet">
<link href="css/font-awesome.min.css" rel="stylesheet">
<script src="js/jquery-3.1.1.min.js"></script>


 <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
      <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

      <![endif]-->

 <!--[if IE]>
      <link href="css/ie.css" rel="stylesheet">
<![endif]-->

<!--[if lt IE 10]>
	<script type="text/javascript" src="js/placeholder.min.js"></script>
<script type="text/javascript">
    $(function() {
    	$('input, textarea').placeholder();
    });
</script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-117243745-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-117243745-1');
</script>

	
	
<![endif]-->	