<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title>Order Confirmation | Domain-b </title>
      <?php include("head.php"); ?>
     
   </head>
   <body class="">
      <!-- Body start -->
      <?php include("header1.php"); ?>
     	 <!-- Breadcrumb Section start -->
			<div class="container">
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 hidden-xs text-center">
						<div class="breadcrumb-nav flat">
							<a href="#" class="active">CHECKOUT</a>
							<a href="#" class="active"> YOUR DETAILS  </a>
							<a href="#" class="active">PAYMENT</a>
							<a href="#" class="active">CONFIRMATION</a>
						</div>
					</div>
				</div>
			</div>
		<!-- Breadcrumb section end -->

      <!--order confirmation start here-->
	  	<div class="container">
	  		<h2 class="login-maintitle confirmation">Order confirmation</h2>
	  			<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		  				<p class="thanks-text-confirmation">Thank you for your order</p>
	  					<p class="black-text">Order number:<span class="bold-text">JALKJ65469ASDRF</span></p>
	  					<p class="black-text">You will receive the email confirmation to <strong>lokesh.thakur@ticworks.com</strong>. Haven't received it. <span class="blue-text"><a href="#"><strong>Click here to resend mail confirmation.</strong></a></span></p>
		  			<!--product detail section start here-->
		  				<div class="table-responsive">          
							  <table class="table shopping-cart">
							    <thead class="heading-section">
							      <tr class="product-title">
							        
							        <th class="col-lg-7">product</th>
							        <th class="col-lg-2 text-center">quantity</th>
							        <th class="text-right col-lg-3"><span class="text-price">price</span></th>
							       </tr>
							    </thead>
							    <tbody class="product-details-section">
							      <tr>
							        
							        <td>
							        	<p class="product-detail confirmation-page">Biological Identification. Woodhead Publishing Series in Electronic and Optical Materials</p>
							        	<p class="user-text">Single user</p>
							    	</td>
							        <td class="text-center">1</td>
							        <td><p class="product-price">US$ 3,950.00</p></td>
							     </tr>
							    </tbody>
							    <tbody class="product-details-section">
							      <tr>
							      	<td><p class="product-detail confirmation-page">Cancer Molecular Biomarkers 2014: A Global Market Study</p>
							        <p class="user-text">Single user</p></td>
							        <td class="text-center">1</td>
							        <td><p class="product-price">US$ 2,150.00</p></td>
							     </tr>
							    </tbody>
							  </table>
						</div>
						<div class="row">
		  					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			  					<h5 class="fianl-cost pull-right">Current total: <strong>US$ 6,100.00</strong></h5>
			  				</div>
			  			</div>  
					  <!--product detail section end here-->
					  	<!--billing details-->
						  	<div class="billing-details">
						  		<p class="black-text"><strong>Product downdload link will be emailed to: lokesh.thakur@ticworks.com</strong></p>
						  		<p class="black-text billing-details">Billing details</p>
						  		<div class="row">
						  			<div class="col-lg-4">
						  			<div class="table-responsive">          
									  	<table class="table order-confirmation">
									    <thead>
									      <tr>
									       <td class="col-lg-1 col-md-1 col-sm-1 col-xs-1"><strong>Payment type</strong> :</td>
									        <td class="col-lg-2 col-md-2 col-sm-2 col-xs-2">Credit card payment</td>
									      </tr>
									    </thead>
									    <tbody>
									      <tr>
									       <td><strong>Name: </strong></td>
									        <td>Lokesh Thakur</td>
									       </tr>
									       <tr>
									       <td><strong>Organization:</strong></td>
									        <td>TIC</td>
									       </tr>
									       <tr>
									       <td><strong>Contact number:</strong></td>
									        <td>+91 6989431855</td>
									       </tr>

									       <tr>
									       <td><strong>Address:</strong></td>
									        <td>6598 Afasdf asdf asdf</td>
									       </tr>
									       <tr>
									       <td><strong>City: </strong></td>
									        <td>Navi mumbai</td>
									       </tr>
									       <tr>
									       <td><strong>Country:</strong></td>
									        <td>India</td>
									       </tr>
									       <tr>
									       <td><strong>Zip/post:</strong></td>
									        <td>400615</td>
									       </tr>
									    </tbody>
									  </table>
								  	</div>
								  </div>
								</div>
						  		</div>
					  	<!--end billing details -->
					</div>
		  		</div>
		  		<div class="row">
		  			<!--buttons start here-->
			  		<div class="col-lg-offset-3 col-lg-3  col-md-3  col-sm-4 col-xs-12">
			  			<button type="button" class="btn btn-default green-button confirmation">Print summary <span class="glyphicon glyphicon glyphicon-print" aria-hidden="true"></span> </button>
			  		</div>
			  		<div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
			  			<button type="button" class="btn btn-default green-button confirmation">Download invoice <span class="glyphicon glyphicon glyphicon-save" aria-hidden="true"></span> </button>
			  		</div>
			  		<!--buttons end here-->
		  		</div>


		  	<!--footer online icons-->
		  	<div class="row online-icon-section">
	  			<div class="col-lg-6 col-sm-4 col-xs-6 pull-left ">
	  				<div class="secured-img"><img src="images/secured-img.png" alt="secured-by"></div>
	  			</div>
	  			<div class="col-lg-6 col-sm-8 col-xs-12">
					<div class="row">
						<div class="col-lg-7 col-md-6 col-sm-7 col-xs-8">
							<p class="credit-card-text">We accept all major credit cards!</p>
							<ul class="list-inline">
								<li><img src="images/icon_1.jpg" alt="secured-by"></li>
								<li><img src="images/icon_2.jpg" alt="secured-by"></li>
								<li><img src="images/icon_3.jpg" alt="secured-by"></li>
								<li><img src="images/icon_4.jpg" alt="secured-by"></li>
								<li><img src="images/icon_5.jpg" alt="secured-by"></li>
							</ul>
						</div>
						<div class="col-lg-5 col-md-6 col-sm-5 col-xs-4">
							<p class="credit-card-text">No PayPal account required</p>
							<div class="paypal-img">
								<img src="images/icon_6.jpg" alt="secured-by">
							</div>
						</div>
				  	</div>
				</div>
		  	</div>
		  	<!--footer online icons end here -->
	  	</div>
	  	<!--end login here-->
	  	
      <?php include("footer.php"); ?>
      <?php include("script.php"); ?>
	   <script>
			$('.alert-item').imagesLoaded( function() {
				$('.alert-item').masonry({
				  itemSelector: '.alert-box',
				});

				setTimeout(function() {
					$('.alert-item').masonry('layout');
				},500);
			});

		</script>
	  
   </body>
   <!-- Body end -->
</html>