<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title>Home | Domain-b </title>
      <?php include("head.php"); ?>
     
   </head>
   <body class="">
      <!-- Body start -->
      <?php include("header.php"); ?>
	 <!-- Breaking News Start -->
      <div class="breaking-news">
         <div class="container">
            <div class="row">
               <div class="col-lg-9 col-md-10 col-sm-10 col-xs-12">
                  <h4>
                     <span><a href="">BREAKING NEWS:</a></span>
                     <marquee class="marquee" behavior="scroll" direction="left" id="mymarquee">
                        <a href="">Australian prime minister Julia Gillard throws open leadership to an expected challenge by Kevin Rudd.</a>
                     </marquee>
                     <img src="images/double-bar.png"  class="img-responsive-pull-right pause-icon" value="Stop Marquee" onClick="document.getElementById('mymarquee').stop();">
                     <i class="fa fa-play pull-right play-icon" aria-hidden="true" value="Start Marquee" onClick="document.getElementById('mymarquee').start();"></i>
                  </h4>
               </div>
            </div>
         </div>
      </div>
      <!-- Breaking News End -->
	 <!-- container start -->
      <div class="container">
	     <!-- row start -->
         <div class="row">
            <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">
               <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 pull-right remove-padding-xs">
			 <!-- section start -->
                 <section>
		  <!-- Carouse Start -->
                     <div id="carousel-example-generic" class="carousel slide carousel-first" data-ride="carousel">
                        <!-- Indicators -->
                        <ol class="carousel-indicators">
                           <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                           <li data-target="#carousel-example-generic" data-slide-to="1" class=""></li>
                           <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                        </ol>
                        <!-- Wrapper for slides -->
                        <div class="carousel-inner" role="listbox">
                           <div class="item active">
                              <img src="images/slider1.jpg" class="img-responsive" width="">
                              <div class="carousel-caption">
                                 <p>Rio Olympics 2016 Badminton Live Score - PV Sindhu vs Nozomi Okuhara: Always a Battle of Nerves</p>
                              </div>
                           </div>
                           <div class="item">
                              <img src="images/slider1.jpg" class="img-responsive" width="">
                              <div class="carousel-caption">
                                 <p>Rio Olympics 2016 Badminton Live Score - PV Sindhu vs Nozomi Okuhara: Always a Battle of Nerves</p>
                              </div>
                           </div>
                        </div>
                        <!-- Controls -->
                        <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                        <span class="carousel-red-arrow"><img src="images/chev-left.png" class="img-responsive"></span>
                        <span class="sr-only">Previous</span>
                        </a>
                        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                        <span class="carousel-red-arrow" aria-hidden="true"><img src="images/chev-right.png" class="img-responsive"></span>
                        <span class="sr-only">Next</span>
                        </a>
                     </div>
                     <!-- Carouse End -->
                  </section>
                  <!-- section end -->
                  <!-- News headlines -->
				      <!-- section start -->
                  <section class="news-headlines">
                     <h2>NEWS HEADLINES</h2>
					 <!-- carousel start -->
                     <div id="carousel-example-generic1" class="carousel slide" data-ride="carousel" data-interval="false">
                        <!-- Indicators -->
                        <ol class="carousel-indicators">
                           <li data-target="#carousel-example-generic1" data-slide-to="0" class="active"></li>
                           <li data-target="#carousel-example-generic1" data-slide-to="1"></li>
                           <li data-target="#carousel-example-generic1" data-slide-to="2"></li>
                           <li data-target="#carousel-example-generic1" data-slide-to="3"></li>
                           <li data-target="#carousel-example-generic1" data-slide-to="4"></li>
                        </ol>
                        <!-- Wrapper for slides -->
                        <div class="carousel-inner" role="listbox">
                           <div class="item active">
                                 <div class="news-carousel col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <h3><a href="">Toshiba Medical enters race to buy Takeda Pharmaceutical's chemical unit</a></h3>
                                    <p>Toshiba Medical Systems has entered the race to buy Wako Pure Chemicals Industries Ltd, a chemicals unit of Takeda Pharmaceutical Co, the ...</p>
                                    <div class="news-time">7:35 am IST</div>
                                    <img src="images/border-bottom.jpg" class="img-responsive">
                                 </div>
                                 <div class="news-carousel col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <h3><a href="">Japanese chipmaker Renesas in talks to buy Intersil for nearly $3 bn</a></h3>
                                    <p>Japanese chipmaker Renesas Electronics Corp today said that it is in talks to buy its US rival Intersil Corp...</p>
                                    <div class="news-time">1h ago</div>
                                    <img src="images/border-bottom.jpg" class="img-responsive">
                                 </div>
								  <div class="clearfix visible-md"></div>
                                 <div class="news-carousel col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <h3><a href="">Viewing torrent sites in India now punishable with 3-yr jail term</a></h3>
                                    <p>Major (Internet Service Providers) ISPs have been asked by the Department of Telecommunications to start blocking torrent sites and serve warnings ...</p>
                                    <div class="news-time">5:00 am IST</div>
                                 </div>
                                 <div class="news-carousel col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <h3><a href="">Pacific sea level predicts global temperature changes</a></h3>
                                    <p>The amount of sea level rise in the Pacific Ocean can be used to  future global surface temperatures, according to a new report led by University...</p>
                                    <div class="news-time">2h ago</div>
                                 </div>
                              </div>
                           <div class="item">
                                 <div class="news-carousel col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <h3><a href="">Toshiba Medical enters race to buy Takeda Pharmaceutical's chemical unit</a></h3>
                                    <p>Toshiba Medical Systems has entered the race to buy Wako Pure Chemicals Industries Ltd, a chemicals unit of Takeda Pharmaceutical Co, the ...</p>
                                    <div class="news-time">7:35 am IST</div>
                                    <img src="images/border-bottom.jpg" class="img-responsive">
                                 </div>
                                 <div class="news-carousel col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <h3><a href="">Japanese chipmaker Renesas in talks to buy Intersil for nearly $3 bn</a></h3>
                                    <p>Japanese chipmaker Renesas Electronics Corp today said that it is in talks to buy its US rival Intersil Corp...</p>
                                    <div class="news-time">1h ago</div>
                                    <img src="images/border-bottom.jpg" class="img-responsive">
                                 </div>
								  <div class="clearfix visible-md"></div>
                                 <div class="news-carousel col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <h3><a href="">Viewing torrent sites in India now punishable with 3-yr jail term</a></h3>
                                    <p>Major (Internet Service Providers) ISPs have been asked by the Department of Telecommunications to start blocking torrent sites and serve warnings ...</p>
                                    <div class="news-time">5:00 am IST</div>
                                 </div>
                                 <div class="news-carousel col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <h3><a href="">Pacific sea level predicts global temperature changes</a></h3>
                                    <p>The amount of sea level rise in the Pacific Ocean can be used to  future global surface temperatures, according to a new report led by University...</p>
                                    <div class="news-time">2h ago</div>
                                 </div>
                           </div>
                           <div class="item">
                                 <div class="news-carousel col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <h3><a href="">Toshiba Medical enters race to buy Takeda Pharmaceutical's chemical unit</a></h3>
                                    <p>Toshiba Medical Systems has entered the race to buy Wako Pure Chemicals Industries Ltd, a chemicals unit of Takeda Pharmaceutical Co, the ...</p>
                                    <div class="news-time">7:35 am IST</div>
                                    <img src="images/border-bottom.jpg" class="img-responsive">
                                 </div>
                                 <div class="news-carousel col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <h3><a href="">Japanese chipmaker Renesas in talks to buy Intersil for nearly $3 bn</a></h3>
                                    <p>Japanese chipmaker Renesas Electronics Corp today said that it is in talks to buy its US rival Intersil Corp...</p>
                                    <div class="news-time">1h ago</div>
                                    <img src="images/border-bottom.jpg" class="img-responsive">
                                 </div>
								  <div class="clearfix visible-md"></div>
                                 <div class="news-carousel col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <h3><a href="">Viewing torrent sites in India now punishable with 3-yr jail term</a></h3>
                                    <p>Major (Internet Service Providers) ISPs have been asked by the Department of Telecommunications to start blocking torrent sites and serve warnings ...</p>
                                    <div class="news-time">5:00 am IST</div>
                                 </div>
                                 <div class="news-carousel col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <h3><a href="">Pacific sea level predicts global temperature changes</a></h3>
                                    <p>The amount of sea level rise in the Pacific Ocean can be used to  future global surface temperatures, according to a new report led by University...</p>
                                    <div class="news-time">2h ago</div>
                                 </div>
                           </div>
                           <div class="item">
                                 <div class="news-carousel col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <h3><a href="">Toshiba Medical enters race to buy Takeda Pharmaceutical's chemical unit</a></h3>
                                    <p>Toshiba Medical Systems has entered the race to buy Wako Pure Chemicals Industries Ltd, a chemicals unit of Takeda Pharmaceutical Co, the ...</p>
                                    <div class="news-time">7:35 am IST</div>
                                    <img src="images/border-bottom.jpg" class="img-responsive">
                                 </div>
                                 <div class="news-carousel col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <h3><a href="">Japanese chipmaker Renesas in talks to buy Intersil for nearly $3 bn</a></h3>
                                    <p>Japanese chipmaker Renesas Electronics Corp today said that it is in talks to buy its US rival Intersil Corp...</p>
                                    <div class="news-time">1h ago</div>
                                    <img src="images/border-bottom.jpg" class="img-responsive">
                                 </div>
								  <div class="clearfix visible-md"></div>
                                 <div class="news-carousel col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <h3><a href="">Viewing torrent sites in India now punishable with 3-yr jail term</a></h3>
                                    <p>Major (Internet Service Providers) ISPs have been asked by the Department of Telecommunications to start blocking torrent sites and serve warnings ...</p>
                                    <div class="news-time">5:00 am IST</div>
                                 </div>
                                 <div class="news-carousel col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <h3><a href="">Pacific sea level predicts global temperature changes</a></h3>
                                    <p>The amount of sea level rise in the Pacific Ocean can be used to  future global surface temperatures, according to a new report led by University...</p>
                                    <div class="news-time">2h ago</div>
                                 </div>
                           </div>
                           <div class="item">
                                 <div class="news-carousel col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <h3><a href="">Toshiba Medical enters race to buy Takeda Pharmaceutical's chemical unit</a></h3>
                                    <p>Toshiba Medical Systems has entered the race to buy Wako Pure Chemicals Industries Ltd, a chemicals unit of Takeda Pharmaceutical Co, the ...</p>
                                    <div class="news-time">7:35 am IST</div>
                                    <img src="images/border-bottom.jpg" class="img-responsive">
                                 </div>
                                 <div class="news-carousel col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <h3><a href="">Japanese chipmaker Renesas in talks to buy Intersil for nearly $3 bn</a></h3>
                                    <p>Japanese chipmaker Renesas Electronics Corp today said that it is in talks to buy its US rival Intersil Corp...</p>
                                    <div class="news-time">1h ago</div>
                                    <img src="images/border-bottom.jpg" class="img-responsive">
                                 </div>
								  <div class="clearfix visible-md"></div>
                                 <div class="news-carousel col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <h3><a href="">Viewing torrent sites in India now punishable with 3-yr jail term</a></h3>
                                    <p>Major (Internet Service Providers) ISPs have been asked by the Department of Telecommunications to start blocking torrent sites and serve warnings ...</p>
                                    <div class="news-time">5:00 am IST</div>
                                 </div>
                                 <div class="news-carousel col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <h3><a href="">Pacific sea level predicts global temperature changes</a></h3>
                                    <p>The amount of sea level rise in the Pacific Ocean can be used to  future global surface temperatures, according to a new report led by University...</p>
                                    <div class="news-time">2h ago</div>
                                 </div>
                           </div>
                        </div>
                     </div>
                     <!-- carousel end -->
                  </section>
                  <!-- section end -->
                  <!-- Featured Section Start -->
				   <!-- section start -->
                  <section class="featured-article-lhs">
                     <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 remove-padding-xs">
                        <h2>FEATURED ARTICLES</h2>
						 <!-- article start -->
                        <article class="featured-article-list">
                           <h3><a href="">Five ways bioengineers want to use 3-D printing</a></h3>
                           <div class="row article-group">
                              <img src="images/featured-article-img1.jpg" class="img-responsive pull-left">
                              <p> Now that 3D printing has made it easier to  generate custom-made prosthetics, bioengineers  are looking ahead at manufacturing actual cellular material</p>
                           </div>
                        </article>
                        <!-- article end -->
						 <!-- article start -->
                        <article class="featured-article-list">
                           <h3><a href="">Drama at the airport</a></h3>
                           <div class="row article-group">
                              <img src="images/featured-article-img2.jpg" class="img-responsive pull-left">
                              <p> Quite often an IAS offi cer's posting in a state is made at the behest of local politicians close to the chief minister, with many an officer unwit tingly caught in the crossfire between rival political factions, says former IAS officer Vivek Agnihotri, who retired as the secretary general  of the Rajya Sabha </p>
                           </div>
                        </article>
                        <!-- article end -->
						 <!--article start-->
                        <article class="featured-article-list">
                           <h3><a href="">Retracing the journey of Indian advertising</a></h3>
                           <div class="row article-group">
                              <img src="images/featured-article-img3.jpg" class="img-responsive pull-left">
                              <p> Ambi Parameswaran, brand strategist, former ad man and founder of brandbuilding.com, cum author talks to Swetha Amit about India's 50 year old journey in advertising </p>
                           </div>
                        </article>
                        <!-- article end-->
						 <!-- article start -->
                        <article class="featured-article-list">
                           <h3><a href="">A fast-paced game of Monopoly</a></h3>
                           <div class="row article-group">
                              <img src="images/featured-article-img4.jpg" class="img-responsive pull-left">
                              <p> Ashwin Sanghi, best-sell ing author and entrepre neur talks to Swetha Amit about his new release The Sialkot Saga - a business thriller, and his philosophies reflected in this novel </p>
                           </div>
                        </article>
                        <!-- article end -->
						<!-- article start -->
                        <article class="featured-article-list">
                           <h3><a href="">Journey from darkness to the spotlight</a></h3>
                           <div class="row article-group">
                              <img src="images/featured-article-img5.jpg" class="img-responsive pull-left">
                              <p> Laxman Gaikwad, winner of the Sahitya Akademi award, talks to Swetha Amit about his journey from a member of a community of branded criminals to a well-known Marathi writer</p>
                           </div>
                        </article>
                        <!-- article end -->
                        <div class="red-button martp-27">
                           <a href="" class="btn btn-default">MORE FEATURED ARTICLES</a>
                        </div>
                     </div>
                  </section>
                  <!-- section end -->
                  <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12 featured-article-rhs">
					 <!--nav-pills ul start -->
                     <ul class="nav nav-pills tabs-autoslide">
                        <li class="active"><a data-toggle="pill" href="#aero">Aero</a></li>
                        <li><a data-toggle="pill" href="#auto">Auto</a></li>
                        <li><a data-toggle="pill" href="#economy">Economy</a></li>
                        <li><a data-toggle="pill" href="#it">IT</a></li>
                        <li><a data-toggle="pill" href="#ma">M&A</a></li>
                        <li><a data-toggle="pill" href="#technology">Technology</a></li>
                     </ul>
                     <!--nav-pills ul end-->
                     <div class="tab-content">
                        <div id="aero" class="tab-pane fade in active">
						 <!-- 1tab ul start-->
                           <ul class="list-unstyled">
                              <li><a href="">Lyft rejects acquisition bid by GM</a></li>
                              <li><img src="images/gray-border-bottom.png" class="img-responsive"></li>
                              <li><a href="">Renault to launch Kwid 1.0-litre ahead of AMT variant</a></li>
                              <li><img src="images/gray-border-bottom.png" class="img-responsive"></li>
                              <li><a href="">Over 100 mn Volkswagen cars sold since 1995 vulnerable to hacks: study</a></li>
                              <li><img src="images/gray-border-bottom.png" class="img-responsive"></li>
                              <li><a href="">Ford slashes prices of Aspire, Figo by up to Rs91,000</a></li>
                              <li><img src="images/gray-border-bottom.png" class="img-responsive"></li>
                              <li><a href="">Up close with the New BMW Z4 S Drive35is</a> </li>
                           </ul>
                           <!-- tab ul end -->
                           <div class="more-button">
                              <a href="" class="btn btn-default">MORE</a>
                           </div>
                        </div>
                        <div id="auto" class="tab-pane fade">
					 <!-- 2tab ul start-->
                           <ul class="list-unstyled">
                              <li><a href="">Lyft rejects acquisition bid by GM</a></li>
                              <li><img src="images/gray-border-bottom.png" class="img-responsive"></li>
                              <li><a href="">Renault to launch Kwid 1.0-litre ahead of AMT variant</a></li>
                              <li><img src="images/gray-border-bottom.png" class="img-responsive"></li>
                              <li><a href="">Over 100 mn Volkswagen cars sold since 1995 vulnerable to hacks: study</a></li>
                              <li><img src="images/gray-border-bottom.png" class="img-responsive"></li>
                              <li><a href="">Ford slashes prices of Aspire, Figo by up to Rs91,000</a></li>
                              <li><img src="images/gray-border-bottom.png" class="img-responsive"></li>
                              <li><a href="">Up close with the New BMW Z4 S Drive35is</a> </li>
                           </ul>
                           <!-- 2tab ul end -->
                           <div class="more-button">
                              <a href="" class="btn btn-default">MORE</a>
                           </div>
                        </div>
                        <div id="economy" class="tab-pane fade">
						 <!-- 3tab ul start -->
                           <ul class="list-unstyled">
                              <li><a href="">Lyft rejects acquisition bid by GM</a></li>
                              <li><img src="images/gray-border-bottom.png" class="img-responsive"></li>
                              <li><a href="">Renault to launch Kwid 1.0-litre ahead of AMT variant</a></li>
                              <li><img src="images/gray-border-bottom.png" class="img-responsive"></li>
                              <li><a href="">Over 100 mn Volkswagen cars sold since 1995 vulnerable to hacks: study</a></li>
                              <li><img src="images/gray-border-bottom.png" class="img-responsive"></li>
                              <li><a href="">Ford slashes prices of Aspire, Figo by up to Rs91,000</a></li>
                              <li><img src="images/gray-border-bottom.png" class="img-responsive"></li>
                              <li><a href="">Up close with the New BMW Z4 S Drive35is</a> </li>
                           </ul>
                           <!-- 3tab ul end -->
                           <div class="more-button">
                              <a href="" class="btn btn-default">MORE</a>
                           </div>
                        </div>
                        <div id="it" class="tab-pane fade">
						 <!-- 4tab ul start -->
                           <ul class="list-unstyled">
                              <li><a href="">Lyft rejects acquisition bid by GM</a></li>
                              <li><img src="images/gray-border-bottom.png" class="img-responsive"></li>
                              <li><a href="">Renault to launch Kwid 1.0-litre ahead of AMT variant</a></li>
                              <li><img src="images/gray-border-bottom.png" class="img-responsive"></li>
                              <li><a href="">Over 100 mn Volkswagen cars sold since 1995 vulnerable to hacks: study</a></li>
                              <li><img src="images/gray-border-bottom.png" class="img-responsive"></li>
                              <li><a href="">Ford slashes prices of Aspire, Figo by up to Rs91,000</a></li>
                              <li><img src="images/gray-border-bottom.png" class="img-responsive"></li>
                              <li><a href="">Up close with the New BMW Z4 S Drive35is</a> </li>
                           </ul>
                           <!-- 4tab ul end -->
                           <div class="more-button">
                              <a href="" class="btn btn-default">MORE</a>
                           </div>
                        </div>
                        <div id="ma" class="tab-pane fade">
						  <!-- 5tab ul start -->
                           <ul class="list-unstyled">
                              <li><a href="">Lyft rejects acquisition bid by GM</a></li>
                              <li><img src="images/gray-border-bottom.png" class="img-responsive"></li>
                              <li><a href="">Renault to launch Kwid 1.0-litre ahead of AMT variant</a></li>
                              <li><img src="images/gray-border-bottom.png" class="img-responsive"></li>
                              <li><a href="">Over 100 mn Volkswagen cars sold since 1995 vulnerable to hacks: study</a></li>
                              <li><img src="images/gray-border-bottom.png" class="img-responsive"></li>
                              <li><a href="">Ford slashes prices of Aspire, Figo by up to Rs91,000</a></li>
                              <li><img src="images/gray-border-bottom.png" class="img-responsive"></li>
                              <li><a href="">Up close with the New BMW Z4 S Drive35is</a> </li>
                           </ul>
                           <!-- 5tab ul end -->
                           <div class="more-button">
                              <a href="" class="btn btn-default">MORE</a>
                           </div>
                        </div>
                        <div id="technology" class="tab-pane fade">
					 <!-- 6tab ul start -->
                           <ul class="list-unstyled">
                              <li><a href="">Lyft rejects acquisition bid by GM</a></li>
                              <li><img src="images/gray-border-bottom.png" class="img-responsive"></li>
                              <li><a href="">Renault to launch Kwid 1.0-litre ahead of AMT variant</a></li>
                              <li><img src="images/gray-border-bottom.png" class="img-responsive"></li>
                              <li><a href="">Over 100 mn Volkswagen cars sold since 1995 vulnerable to hacks: study</a></li>
                              <li><img src="images/gray-border-bottom.png" class="img-responsive"></li>
                              <li><a href="">Ford slashes prices of Aspire, Figo by up to Rs91,000</a></li>
                              <li><img src="images/gray-border-bottom.png" class="img-responsive"></li>
                              <li><a href="">Up close with the New BMW Z4 S Drive35is</a> </li>
                           </ul>
                           <!-- 6tab ul start -->
                           <div class="more-button">
                              <a href="" class="btn btn-default">MORE</a>
                           </div>
                        </div>
                     </div>
                     <!-- Reviews Section -->
                     <div class="reviews">
                        <h2>REVIEWS</h2>
                        <div class="row">
                           <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 reviews-img-listing">
                              <a href=""><img  src="images/review-img1.jpg" class="img-responsive img-center"></a>
                           </div>
                           <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 reviews-img-listing">
                              <a href=""><img src="images/review-img2.jpg" class="img-responsive img-center"></a>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 reviews-img-listing">
                              <a href=""><img src="images/review-img3.jpg" class="img-responsive img-center"></a>
                           </div>
                           <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 reviews-img-listing">
                              <a href=""><img src="images/review-img4.jpg" class="img-responsive img-center"></a>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                              <a href=""><img src="images/review-img5.jpg" class="img-responsive img-center"></a>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- Latest News start (grid 1) -->
               <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 latest-news remove-padding-left remove-padding-xs">
                  <h4>LATEST NEWS</h4>
                  <div class="latest-news-section">
                     <div class="row latest-news-list">
                        <img src="images/latest-new1.jpg" class="pull-left img-responsive">
                        <h3><a href="">Moody's keeps  growth orecast  pegged at 7.5%</a></h3>
                        <p> While the outlook for India remains unchanged and China's has been revised slightly upwards for 2016, Moody's warns of uncertainty due to policy changes following the US presidential election</p>
                     </div>
                     <div class="date">22 August 2016</div>
                     <img src="images/border-bottom.jpg" class="img-responsive">
                  </div>
                  <div class="latest-news-section">
                     <div class="row latest-news-list">
                        <img src="images/latest-new2.jpg" class="pull-left img-responsive">
                        <h3><a href="">Pfizer in advanced talks to buy cancer drug specialist Medivation for nearly $14 bn</a></h3>
                        <p>A successful deal  would be Pfizer's biggest acquisition since it purchased biologic drug specialist Hospira Inc last year for about $17 billion</p>
                     </div>
                     <div class="date">18 August 2016</div>
                     <img src="images/border-bottom.jpg" class="img-responsive">
                  </div>
                  <div class="latest-news-section">
                     <div class="row latest-news-list">
                        <img src="images/latest-new3.jpg" class="pull-left img-responsive">
                        <h3><a href="">Ford targets fully autonomous car production by 2021 </a> </h3>
                        <p>Ford is investing in new technologies and collaborating with four start-ups to deliver high-volume, fully autonomous vehicle for ride sharing in 2021</p>
                     </div>
                     <div class="date">18 August 2016</div>
                     <img src="images/border-bottom.jpg" class="img-responsive">
                  </div>
                  <div class="latest-news-section">
                     <div class="row latest-news-list">
                        <h3><a href="">US teen clothing retailer American Apparel explores sale </a></h3>
                        <p>US teen clothing retailer American Apparel has hired investment bank Houlihan Lokey Inc to explore a sale, Reuters yesterday reported, citing people familiar with the matter.</p>
                     </div>
                     <div class="date">18 August 2016</div>
                     <img src="images/border-bottom.jpg" class="img-responsive">
                  </div>
                  <!-- Press Releases -->
                  <div  class="press-releases">
                     <h2>press releases</h2>
                     <div id="wrapper">
                        <div class="slim-scrollbar" id="press-releases-list">
                           <div class="force-overflow">
						     <!-- press-release ul start -->
                              <ul class="list-unstyled">
                                 <li><a href="">Apple Announces Environmental Progress in China & Applauds Supplier Commitment to Clean Energy</a></li>
                                 <li><a href="">Mahindra launches electric car `e20' with Rs5.96 lakh price tag</a></li>
                                 <li><a href="">Mylan Launches Generic Version of Xopenex; Inhalation Solution </a></li>
                                 <li><a href="">PPG announces North American launch of TRIBRID lenses </a></li>
                                 <li><a href="">Essar Energy appoints Deepak Maheshwari as Chief Financial Officer </a></li>
                                 <li><a href="">Apple & Donate Life America Bring National Organ Donor Registration to iPhone </a></li>
                                 <li><a href="">Oracle Introduces New Release of Oracle's Primavera P6 Analytics</a></li>
                                 <li><a href="">Mahindra e2o set to redefine the Future of Mobility </a></li>
                                 <li><a href="">Ericsson and Telstra successfully trial 1Tbps optical link </a></li>
                                 <li><a href="">Small Businesses Lag Far Behind Big Companies in Digital Advertising </a></li>
                                 <li><a href="">Ericsson and Telstra successfully trial 1Tbps optical link </a></li>
                                 <li><a href="">Lockheed Martin to Continue Providing Life Sciences Support To NASA </a></li>
                                 <li><a href="">Mobile Devices and DVRs Shifting Global Media Consumption </a></li>
                                 <li><a href="">Apple Reports Second Quarter Results </a></li>
                                 <li><a href="">Raytheon helps Missile Defense Agency counter ballistic missile threat with delivery of 8th AN/TPY-2 radar </a></li>
                                 <li><a href="">Singapore Airlines and Tourism & Events Queensland launch the 'Holiday Money Can't Buy' contest </a></li>
                                 <li><a href="">Mobile Devices and DVRs Shifting Global Media Consumption </a></li>
                                 <li><a href="">Ericsson and Telstra successfully trial 1Tbps optical link </a></li>
                              </ul>
                              <!-- press-release ul end -->
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="red-button">
                     <a href="" class="btn btn-default">MORE RELEASES</a>
                  </div>
               </div>
               <!-- Latest News end -->
               <!-- Buy Market Research Report start -->
               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 remove-padding-left remove-padding-xs width-sm-124">
                  <div class="market-research-report">
                     <h2>BUY market research reports <a href="" class="btn btn-default pull-right hidden-xs">EXPLORE MORE <img src="images/explore-btn.png" class=""></a></h2>
                     <div id="carousel-example-generic2" class="carousel slide" data-ride="carousel">
						 <!-- Indicators -->
						 <ol class="carousel-indicators">
							<li data-target="#carousel-example-generic2" data-slide-to="0" class="active"></li>
							<li data-target="#carousel-example-generic2" data-slide-to="1"></li>
						 </ol>
                        <!-- Wrapper for slides -->
                        <div class="carousel-inner market-research-listing" role="listbox">
                           <div class="item active">
                              <h3>Markets Research Information Reports by Sectors</h3>
                              <div class="col-lg-4 col-md-3 col-sm-6 col-xs-12 remove-padding-left">
                                 <h4>Pharmaceuticals & Healthcare</h4>
                                 <ul class="list-unstyled">
                                    <li><a href="">Biotechnology</a></li>
                                    <li><a href="">Healthcare and Medical Devices</a></li>
                                    <li><a href="">Pharmaceuticals</a></li>
                                 </ul>
                                 <h4>Business and Finance</h4>
                                 <ul class="list-unstyled">
                                    <li><a href="">Business</a></li>
                                    <li><a href=""> Banking and Financial Services</a></li>
                                 </ul>
                                 <h4>Retail</h4>
                                 <ul class="list-unstyled">
                                    <li><a href="">Retail</a></li>
                                    <li><a href="">Consumer goods</a></li>
                                    <li><a href="">Food & Beverage</a></li>
                                 </ul>
                              </div>
                              <div class="col-lg-2 col-md-3 col-sm-6 col-xs-12">
                                 <h4>Industry</h4>
                                 <ul class="list-unstyled">
                                    <li><a href="">Agriculture</a></li>
                                    <li><a href="">Construction</a></li>
                                    <li><a href="">Energy</a></li>
                                    <li><a href="">Metallurgy</a></li>
                                    <li><a href="">Mineral raw materials</a></li>
                                    <li><a href="">Machinery & equipment</a></li>
                                    <li><a href="">Defence</a></li>
                                    <li><a href="">Packaging</a></li>
                                    <li><a href="">Publishing</a></li>
                                    <li><a href="">Pulp & paper</a></li>
                                    <li><a href="">Vehicle</a></li>
                                 </ul>
                              </div>
                              <div class="clearfix visible-sm"></div>
                              <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                 <h4>Chemicals</h4>
                                 <ul class="list-unstyled">
                                    <li><a href="">Organic Chemicals</a> </li>
                                    <li><a href="">Inorganic Chemicals</a></li>
                                    <li><a href="">Chemical Company</a> </li>
                                    <li><a href="">Reports </a></li>
                                    <li><a href="">Chemical Reports by CAS </a></li>
                                    <li><a href="">Machinery & equipment</a></li>
                                    <li><a href="">Number </a></li>
                                    <li><a href="">Composite Materials </a></li>
                                    <li><a href="">Oils & Lubricants</a></li>
                                 </ul>
                                 <h4>IT & Technology</h4>
                                 <ul class="list-unstyled">
                                    <li><a href="">Consumer Electronics</a></li>
                                    <li><a href="">Hardware</a></li>
                                    <li><a href="">Software</a></li>
                                    <li><a href="">Telecommunications</a></li>
                                 </ul>
                              </div>
                              <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                 <h4>Life sciences</h4>
                                 <ul class="list-unstyled">
                                    <li><a href="">Medical Devices</a></li>
                                    <li><a href="">Biotechnology</a></li>
                                    <li><a href="">Diagnostics & Diseases</a></li>
                                    <li><a href="">Healthcare </a></li>
                                    <li><a href="">Pharmaceuticals</a></li>
                                    <li><a href="">Medical Products</a></li>
                                    <li><a href="">Veterinary</a></li>
                                 </ul>
                                 <h4>Services</h4>
                                 <ul class="list-unstyled">
                                    <li><a href="">Freight & Trucking</a></li>
                                    <li><a href="">Media & Entertainment</a></li>
                                    <li><a href="">Food Service</a></li>
                                    <li><a href="">Consumer Services</a></li>
                                    <li><a href="">Education</a></li>
                                 </ul>
                              </div>
                           </div>
                           <div class="item">
                              <h3>Markets Research Information Reports by Sectors</h3>
                              <div class="col-lg-4 col-md-3 col-sm-6 col-xs-12 remove-padding-left">
                                 <h4>Pharmaceuticals & Healthcare</h4>
                                 <ul class="list-unstyled">
                                    <li><a href="">Biotechnology</a></li>
                                    <li><a href="">Healthcare and Medical Devices</a></li>
                                    <li><a href="">Pharmaceuticals</a></li>
                                 </ul>
                                 <h4>Business and Finance</h4>
                                 <ul class="list-unstyled">
                                    <li><a href="">Business</a></li>
                                    <li><a href=""> Banking and Financial Services</a></li>
                                 </ul>
                                 <h4>Retail</h4>
                                 <ul class="list-unstyled">
                                    <li><a href="">Retail</a></li>
                                    <li><a href="">Consumer goods</a></li>
                                    <li><a href="">Food & Beverage</a></li>
                                 </ul>
                              </div>
                              <div class="col-lg-2 col-md-3 col-sm-6 col-xs-12">
                                 <h4>Industry</h4>
                                 <ul class="list-unstyled">
                                    <li><a href="">Agriculture</a></li>
                                    <li><a href="">Construction</a></li>
                                    <li><a href="">Energy</a></li>
                                    <li><a href="">Metallurgy</a></li>
                                    <li><a href="">Mineral raw materials</a></li>
                                    <li><a href="">Machinery & equipment</a></li>
                                    <li><a href="">Defence</a></li>
                                    <li><a href="">Packaging</a></li>
                                    <li><a href="">Publishing</a></li>
                                    <li><a href="">Pulp & paper</a></li>
                                    <li><a href="">Vehicle</a></li>
                                 </ul>
                              </div>
                              <div class="clearfix visible-sm"></div>
                              <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                 <h4>Chemicals</h4>
                                 <ul class="list-unstyled">
                                    <li><a href="">Organic Chemicals </a></li>
                                    <li><a href="">Inorganic Chemicals</a></li>
                                    <li><a href="">Chemical Company </a></li>
                                    <li><a href="">Reports </a></li>
                                    <li><a href="">Chemical Reports by CAS </a></li>
                                    <li><a href="">Machinery & equipment</a></li>
                                    <li><a href="">Number </a></li>
                                    <li><a href="">Composite Materials </a></li>
                                    <li><a href="">Oils & Lubricants</a></li>
                                 </ul>
                                 <h4>IT & Technology</h4>
                                 <ul class="list-unstyled">
                                    <li><a href="">Consumer Electronics</a></li>
                                    <li><a href="">Hardware</a></li>
                                    <li><a href="">Software</a></li>
                                    <li><a href="">Telecommunications</a></li>
                                 </ul>
                              </div>
                              <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                 <h4>Life sciences</h4>
                                 <ul class="list-unstyled">
                                    <li><a href="">Medical Devices</a></li>
                                    <li><a href="">Biotechnology</a></li>
                                    <li><a href="">Diagnostics & Diseases</a></li>
                                    <li><a href="">Healthcare </a></li>
                                    <li><a href="">Pharmaceuticals</a></li>
                                    <li><a href="">Medical Products</a></li>
                                    <li><a href="">Veterinary</a></li>
                                 </ul>
                                 <h4>Services</h4>
                                 <ul class="list-unstyled">
                                    <li><a href="">Freight & Trucking</a></li>
                                    <li><a href="">Media & Entertainment</a></li>
                                    <li><a href="">Food Service</a></li>
                                    <li><a href="">Consumer Services</a></li>
                                    <li><a href="">Education</a></li>
                                 </ul>
                              </div>
                           </div>
                        </div>
                        <!-- Carousel Controls -->
                        <div id="" class="market-research-carousel-icon">
                           <a class="left carousel-control" href="#carousel-example-generic2" role="button" data-slide="prev">
                           <span class="carousel-red-arrow"><img src="images/chev-left.png" class="img-responsive"></span>
                           <span class="sr-only">Previous</span>
                           </a>
                           <a class="right carousel-control" href="#carousel-example-generic2" role="button" data-slide="next">
                           <span class="carousel-red-arrow" aria-hidden="true"><img src="images/chev-right.png" class="img-responsive"></span>
                           <span class="sr-only">Next</span>
                           </a>
                        </div>
                     </div>
                     <h2 class="explore-display-mobile visible-xs"><a href="" class="btn btn-default visible-xs">EXPLORE MORE <img src="images/explore-btn.png" class=""></a></h2>
                  </div>
               </div>
               <!-- Buy Market Research Report end -->
               <!-- Domail-b shopping start -->
			   <div class="width-sm-124">
               <div class="col-lg-10 col-md-9 col-sm-8 col-xs-12 domain-b-shopping-rhs pull-right remove-padding-xs">
                  <div class="domain-b-shopping-header">
                     <img src="images/domain-b-shopping.jpg" class="img-responsive domain-b-shopping-img">
                  </div>
                  <div class="">
                     <img src="images/product-img1.jpg" class="img-responsive">
                     <p class="explore-more"><a href="" class="pull-right">EXPLORE MORE<img src="images/explore-arrow.jpg"></a></p>
                  </div>
                  <div class="">
                     <img src="images/product-img1.jpg" class="img-responsive">
                     <p class="explore-more"><a href="" class="pull-right">EXPLORE MORE<img src="images/explore-arrow.jpg"></a></p>
                  </div>
               </div>
               <div class="col-lg-2 col-md-3 col-sm-4 col-xs-12 remove-padding-left remove-padding-xs">
                  <div class="domain-b-shopping-lhs">
                     <h2>case studies</h2>
                     <div class="case-studies row">
                        <h3><a href="">'The curse of many publishers is they hold on to print for too long'</h3>
                        <img src="images/case-study-img1.jpg" class="img-responsive img-left-mobile"></a>
                        <p>Digital disruption stalks the media industry. New technologies threaten to make old ones obsolete every week. So publishers		covering the technology industry are closer to the eye of the storm...</p>
                     </div>
                     <div class="case-studies row border-bottom-none">
                        <h3><a href="">Unlocking the Digital-Marketing Potential of Small Businesses </h3>
                        <img src="images/case-study-img2.jpg" class="img-responsive img-left-mobile"></a>
                        <p>Though an estimated 23 million small  businesses power the  US economay, these businesses are failing to keep up with big companies in the online advertising...</p>
                        <div class="more-details"><a href="">MORE<img src="images/more-arrow.png"></a></div>
                     </div>
                  </div>
               </div>
               </div>
            </div>
            <!-- rhs section strt (grid 3) -->
            <aside>
               <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12 iframe-section1">
                  <div class="iframe-img-listing">
                     <div class="poll-design">
                        <h2>Poll</h2>
                        <p>It is not just legislation, but the will to implement law is crucial to curbing crimes against women</p>
                        <!-- Multiple Radios buttons -->
                        <div class="form-group">
                           <div class="radio">
                              <label for="radios-0">
                              <input type="radio" name="radios" id="radios-0" value="1" checked="checked">
                              Agree
                              </label>
                           </div>
                           <div class="radio">
                              <label for="radios-1">
                              <input type="radio" name="radios" id="radios-1" value="2">
                              Disagree
                              </label>
                           </div>
                           <div class="radio">
                              <label for="radios-1">
                              <input type="radio" name="radios" id="radios-1" value="2">
                              Can't Say
                              </label>
                           </div>
                           <div class="vote-button">
                              <a href="" class="btn btn-default">Vote</a>
                           </div>
                           <div class="vote-results">
                              <p><a href="">Current result</a></p>
                              <p><a href="">See previous results</a></p>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="iframe-img-listing">
                     <img src="images/smg-rocks.jpg" class="img-responsive img-center">
                  </div>
                  <div class="iframe-img-listing">
                     <img src="images/icecream.jpg" class="img-responsive img-center">
                  </div>
                  <div class="iframe-img-listing">
                     <img src="images/mela.jpg" class="img-responsive img-center">
                  </div>
               </div>
            </aside>
            <!-- rhs section end -->
         </div>
         <!-- row End -->
      </div>
      <!-- container End -->
      <?php include("footer.php"); ?>
      <?php include("script.php"); ?>
	  <script>
		$('#carousel-example-generic, #carousel-example-generic2').carousel({
		  interval: 8000
		})
	  </script>
   </body>
   <!-- Body end -->
</html>