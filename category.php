<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Checkout | Domain-b </title>
		<?php include("head.php"); ?>
	  
	</head>
	<body class="">
		<!-- Body start -->
		<?php include("header.php"); ?>
			<!-- Breadcrumb Section start -->
				<div class="breadcrumb-section">
					 <div class="container">
						  <ol class="breadcrumb">
							 <li><a href="#">Home</a></li>
							 <li><a href="#">Categories    </a></li>
							 <li><a href="#"> Pharmaceuticals and Healthcare</a></li>
						  
						  </ol>
					 </div>
				</div>
		  <!-- Breadcrumb section end -->

		<!--login window start here-->
<div class="container">


<div class="row">

<!--filter start here-->
<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
<section class="filter-all">
<h3 class="filter-title main-heading hidden-sm hidden-xs">filter by</h3>

<div class="visible-xs">
	<h3 class="filter-title main-heading" data-toggle="collapse"  data-target="#demo">filter by <i class="fa fa-plus pull-right" aria-hidden="true"></i></h3> 
</div>


<div class="button-group" id="demo" class="collapse">
<h3 class="filter-title">Category wise</h3>
<div id="filter-all">
<ul class="main-filter navbar-collapse collapse in" aria-expanded="true">

<li><a  role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" data-value="option1" tabIndex="-1">
<input type="checkbox" class="filter-title"/><span class="glyphicon glyphicon-minus"></span><span class="filter-title">Biotechnology</span> <span class="pull-right">2,568</span></a>
</li>
<ul class="list-inline filter" id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne" aria-expanded="true"> 
<li><a href="#"  data-value="option2" tabIndex="-1"><input type="checkbox"/>Genomics <span class="pull-right">661</span></a></li>
<li><a href="#"  data-value="option3" tabIndex="-1"><input type="checkbox"/>Markets<span class="pull-right">854</span></a></li>
<li><a href="#"  data-value="option4" tabIndex="-1"><input type="checkbox"/>Technology<span class="pull-right">777</span></a></li>
<li><a href="#"  data-value="option5" tabIndex="-1"><input type="checkbox"/>Fast facts<span class="pull-right">0</span></a></li>
<li><a href="#"  data-value="option6" tabIndex="-1"><input type="checkbox"/>Companies<span class="pull-right">15</span></a></li>
<li><a href="#"  data-value="option6" tabIndex="-1"><input type="checkbox"/>Proteomics<span class="pull-right">30</span></a></li>
<li><a href="#"  data-value="option6" tabIndex="-1"><input type="checkbox"/>Bioinformatics<span class="pull-right">628</span></a></li>

</ul>
<li><a  role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="true" aria-controls="collapseOne" data-value="option1" tabIndex="-1"><input type="checkbox"/><span class="glyphicon glyphicon-minus"></span><span class="filter-title">Healthcare</span>    <span class="pull-right">15,568</span> </a></li>
<ul class="list-inline filter" id="collapseTwo" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne" aria-expanded="true"> 
<li><a href="#"  data-value="option2" tabIndex="-1"><input type="checkbox"/>Genomics<span class="pull-right">661</span></a></li>
<li><a href="#"  data-value="option3" tabIndex="-1"><input type="checkbox"/>Markets<span class="pull-right">661</span></a></li>
<li><a href="#"  data-value="option4" tabIndex="-1"><input type="checkbox"/>Technology<span class="pull-right">661</span></a></li>
<li><a href="#"  data-value="option5" tabIndex="-1"><input type="checkbox"/>Fast facts<span class="pull-right">661</span></a></li>
<li><a href="#"  data-value="option6" tabIndex="-1"><input type="checkbox"/>Companies<span class="pull-right">661</span></a></li>
<li><a href="#"  data-value="option6" tabIndex="-1"><input type="checkbox"/>Proteomics<span class="pull-right">661</span></a></li>
<li><a href="#"  data-value="option6" tabIndex="-1"><input type="checkbox"/>Bioinformatics<span class="pull-right">661</span></a></li>

</ul>
<li><a  role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="true" aria-controls="collapseOne" data-value="option1" tabIndex="-1"><input type="checkbox"/><span class="glyphicon glyphicon-minus"></span><span class="filter-title">Pharmaceuticals</span>  <span class="pull-right">10,664</span>    </a></li>
<ul class="list-inline filter" id="collapseThree" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne" aria-expanded="true"> 
<li><a href="#"  data-value="option2" tabIndex="-1"><input type="checkbox"/>Genomics<span class="pull-right">661</span></a></li>
<li><a href="#"  data-value="option3" tabIndex="-1"><input type="checkbox"/>Markets<span class="pull-right">661</span></a></li>
<li><a href="#"  data-value="option4" tabIndex="-1"><input type="checkbox"/>Technology<span class="pull-right">661</span></a></li>
<li><a href="#"  data-value="option5" tabIndex="-1"><input type="checkbox"/>Fast facts<span class="pull-right">661</span></a></li>
<li><a href="#"  data-value="option6" tabIndex="-1"><input type="checkbox"/>Companies<span class="pull-right">661</span></a></li>
<li><a href="#"  data-value="option6" tabIndex="-1"><input type="checkbox"/>Proteomics<span class="pull-right">661</span></a></li>
<li><a href="#"  data-value="option6" tabIndex="-1"><input type="checkbox"/>Bioinformatics<span class="pull-right">661</span></a></li>

</ul>
</ul>
</div>

<div class="button-group">
<!--<button type="button" class="btn btn-default btn-sm dropdown-toggle hidden-lg hidden-md hidden-sm visible-xs" data-toggle="dropdown"><span class="glyphicon glyphicon-cog"></span> <span class="caret"></span></button>-->
<h3 class="filter-title">Regions</h3>
<ul class=" filter scroll">
<li><a href="#"  data-value="option1" tabIndex="-1"><input type="checkbox"/>Global<span class="pull-right">1,661</span></a></li>
<li><a href="#"  data-value="option2" tabIndex="-1"><input type="checkbox"/>Asia<span class="pull-right">854</span></a></li>
<li><a href="#"  data-value="option3" tabIndex="-1"><input type="checkbox"/>Africa<span class="pull-right">77</span></a></li>
<li><a href="#"  data-value="option4" tabIndex="-1"><input type="checkbox"/>Europe<span class="pull-right">99</span></a></li>
<li><a href="#"  data-value="option5" tabIndex="-1"><input type="checkbox"/>North America<span class="pull-right">15</span></a></li>
<li><a href="#"  data-value="option6" tabIndex="-1"><input type="checkbox"/>Australia<span class="pull-right">30</span></a></li>
<li><a href="#"  data-value="option6" tabIndex="-1"><input type="checkbox"/>South America<span class="pull-right">28</span></a></li>
</ul>
</div>

<div class="button-group">
<h3 class="filter-title">Countries</h3>
<!--<button type="button" class="btn btn-default btn-sm dropdown-toggle hidden-lg hidden-md hidden-sm visible-xs" data-toggle="dropdown"><span class="glyphicon glyphicon-cog"></span> <span class="caret"></span></button>-->
<ul class="filter scroll">
<li><a href="#"  data-value="option1" tabIndex="-1"><input type="checkbox"/>Algeria<span class="pull-right">66</span></a></li>
<li><a href="#"  data-value="option2" tabIndex="-1"><input type="checkbox"/>Angola<span class="pull-right">5</span></a></li>
<li><a href="#"  data-value="option3" tabIndex="-1"><input type="checkbox"/>Argentina<span class="pull-right">15</span></a></li>
<li><a href="#"  data-value="option4" tabIndex="-1"><input type="checkbox"/>Armenia<span class="pull-right">14</span></a></li>
<li><a href="#"  data-value="option5" tabIndex="-1"><input type="checkbox"/>Australia<span class="pull-right">55</span></a></li>
<li><a href="#"  data-value="option6" tabIndex="-1"><input type="checkbox"/>Austria<span class="pull-right">48</span></a></li>
<li><a href="#"  data-value="option6" tabIndex="-1"><input type="checkbox"/>Azerbaijan<span class="pull-right">76</span></a></li>
<li><a href="#"  data-value="option1" tabIndex="-1"><input type="checkbox"/>Bahrain<span class="pull-right">66</span></a></li>
<li><a href="#"  data-value="option2" tabIndex="-1"><input type="checkbox"/>Bangladesh<span class="pull-right">5</span></a></li>
<li><a href="#"  data-value="option3" tabIndex="-1"><input type="checkbox"/>Belarus<span class="pull-right">15</span></a></li>
<li><a href="#"  data-value="option4" tabIndex="-1"><input type="checkbox"/>Belgium<span class="pull-right">14</span></a></li>
<li><a href="#"  data-value="option5" tabIndex="-1"><input type="checkbox"/>Bosnia and Herze<span class="pull-right">55</span></a></li>
<li><a href="#"  data-value="option6" tabIndex="-1"><input type="checkbox"/>govina<span class="pull-right">48</span></a></li>
<li><a href="#"  data-value="option6" tabIndex="-1"><input type="checkbox"/>Brazil<span class="pull-right">76</span></a></li>
<li><a href="#"  data-value="option6" tabIndex="-1"><input type="checkbox"/>Bulgaria<span class="pull-right">66</span></a></li>
<li><a href="#"  data-value="option6" tabIndex="-1"><input type="checkbox"/>Burma<span class="pull-right">5</span></a></li>
<li><a href="#"  data-value="option6" tabIndex="-1"><input type="checkbox"/>Cambodia<span class="pull-right">15</span></a></li>
<li><a href="#"  data-value="option6" tabIndex="-1"><input type="checkbox"/>Cameroon<span class="pull-right">14</span></a></li>
<li><a href="#"  data-value="option6" tabIndex="-1"><input type="checkbox"/>Canada<span class="pull-right">55</span></a></li>
</ul>
</div>

<div class="button-group">
<h3 class="filter-title">Price range</h3>
<ul class=" filter-price">
<li><div class="radio">
<input type="radio" name="radio1" id="radio1" value="option1" checked="">
<label for="radio1">USD 0 - USD 500 </label><span class="pull-right">661</span>
</div></li>
<li><div class="radio">
<input type="radio" name="radio1" id="radio1" value="option1" checked="">
<label for="radio1">USD 0 - USD 1,000</label><span class="pull-right">661</span>
</div></li>
<li><div class="radio">
<input type="radio" name="radio1" id="radio1" value="option1" checked="">
<label for="radio1">USD 0 - USD 2,000</label><span class="pull-right">661</span>
</div></li>
<li><div class="radio">
<input type="radio" name="radio1" id="radio1" value="option1" checked="">
<label for="radio1">Any Price</label><span class="pull-right">661</span>
</div></li>
</ul>
</div>

<div class="button-group">
<h3 class="filter-title">Published date</h3>
<ul class=" filter-price">
<li><div class="radio">
<input type="radio" name="radio1" id="radio2" value="option1" checked="">
<label for="radio1">Less than 1 month ago</label><span class="pull-right">661</span>
</div></li>
<li><div class="radio">
<input type="radio" name="radio1" id="radio2" value="option1" checked="">
<label for="radio1">Less than 3 months</label><span class="pull-right">661</span>
</div>
<li><div class="radio">
<input type="radio" name="radio1" id="radio2" value="option1" checked="">
<label for="radio1">Less than 1 year</label><span class="pull-right">661</span>
</div></li>
<li><div class="radio">
<input type="radio" name="radio1" id="radio2" value="option1" checked="">
<label for="radio1">All time</label><span class="pull-right">661</span>
</div></li>
</ul>
</div>

</section>
<div class="adds-category hidden-sm hidden-xs">
<a href="#"><img src="images/adds-product.jpg" alt="product-img" class="img-responsive adds-cat"></a>
</div>
</div>
<!--filter end here-->

<!--content here-->
<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
<div class="row">
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
<section class="top-section">
<h2 class="login-maintitle category">Biotechnology</h2>
<div class="category-content">
<p class="category-text">Biotechnology is used to develop products and services for a wide variety of industries and applications, having its biggest direct impact in medicine and healthcare. More than 250 biotechnological healthcare products and vaccines are already available, with many more to come.</p>
</div>
<p class="product-content-heading sub-heading">Price range  |  Published date</p>
</section>
</div>
</div>

<section class="refine-search">
<div class="row">
<div class="col-lg-6 col-md-6 col-sm-6 hidden-xs">
<div class="search_container"> 
<div class="input-group search-input-group">
<input type="text" class="form-control"  placeholder="Refine your search" >
<span class="input-group-addon">
<button type="submit">
	 <span class="glyphicon glyphicon-search category-search"></span>
</button>  
</span>
</div>
</div>
</div>
<div class="col-lg-6 col-md-6 col-sm-6 hidden-xs">
<ul class="list-inline pull-right">
	<li><p  class="black-text">page no </p></li>
	<li><input type="text" class="form-control page-no" placeholder="1 "> </li>
	<li><p  class="black-text">of 20 </p></li>
	<li><p  class="black-text"><span class="go-text">go</span></p></li>
	<li><span class="glyphicon glyphicon-menu-left filter-arrow" aria-hidden="true"></span>
		<span class="glyphicon glyphicon-menu-right filter-arrow" aria-hidden="true"></span>
		<div class="clearfix"></div>
	</li>
</ul>

<!-- <p  class="pull-right black-text">page no <input type="text" class="form-control page-no" placeholder="1 "> of 20 
<span class="go-text">go</span>

<span class="glyphicon glyphicon-menu-left filter-arrow" aria-hidden="true"></span><span class="glyphicon glyphicon-menu-right filter-arrow" aria-hidden="true"></span><div class="clearfix"></div>
</p> -->

</div>
</div>
</section>

<section class="compulsory-fields">
<div class="row">
<p > * Compulsory fields</p>
<!--<p>
<?php echo "Today is " . date("Y/m/d") . "<br>"; ?>

</p>-->
<p class="black-text"><strong>Published date </strong>
<select name="Day" class="cat-date">
<option value="1">3</option>
<option value="2">2</option>
<option selected value="3">dd</option>
</select>
<select name="Month" class="cat-date">
<option value="January">Mar</option>
<option value="February">Feb</option>
<option selected value="March">mm</option>
</select>   
<select name="Year" class="cat-date">
<option value="2013">2011</option>
<option value="2012">2012</option>
<option selected value="2011">yyyy</option>
</select>



to
<select name="Day" class="cat-date">
<option value="1">3</option>
<option value="2">2</option>
<option selected value="3">dd</option>
</select>
<select name="Month" class="cat-date">
<option value="January">Mar</option>
<option value="February">Feb</option>
<option selected value="March">mm</option>
</select>   
<select name="Year" class="cat-date">
<option value="2013">2011</option>
<option value="2012">2012</option>
<option selected value="2011">yyyy</option>
</select>



<button type="button" class="btn btn-default brown-button category-search">search </button> </p>
</div>
</section>
<!--main content start-->
<div class="">          
<table class="table order-confirmation product-page" id="category-table">
<tr>
<thead class="product-heading-back">
<td class="col-lg-8 col-sm-8 col-xs-12"><p class="product-content-title">PRODUCT TITLE</p></td>
<td class="col-lg-2 col-sm-2 hidden-xs"><p class="product-published">PUBLISHED </p></td>
<td class="col-lg-2 col-sm-2 hidden-xs"><p class="product-price-title">Price </p></td>
</thead>
</tr>
<tbody>
<tr class="light-background">
<td>
<p class="product-content-heading">Biotechnology for the Non Biotechnologist (Course, France)</p>
<p class="product-content-text">This comprehensive 3 day course is ideal for Scientists and Non-Scientists needing to understand the basic theory, principles and techniques of 
biotechnology TOPICS COVERED TO INCLUDE: - An introduction...
</p>
<div class="visible-xs category-content">
	 <label>PUBLISHED </label>
	 <p class="publishe-date">Apr-14</p>
	 <p class="product-amount">USD 3,460</p>
	 
</div>
</td>
<td class="hide-content"><p class="publishe-date">Apr-14</p></td>
<td class="hide-content"><p class="product-amount">USD 3,460</p></td>
</tr>
<tr  class="dark-background">
<td>
<p class="product-content-heading">Biological Identification. Woodhead Publishing Series in Electronic and Optical Materials</p>
<p class="product-content-text">Biological identification systems are a vital component in countering the threats to life and health posed by natural pathogens, toxins, and bioterrorism agents. It is essential that such systems are...
</p>
<div class="visible-xs category-content">
	 <label>PUBLISHED </label>
	 <p class="publishe-date">Apr-14</p>
	 <p class="product-amount">USD 230</p>
	 
</div>
</td>
<td><p class="publishe-date">Apr-14</p></td>
<td><p class="product-amount">USD 230</p></td>
</tr>
<tr  class="light-background">
<td>
<p class="product-content-heading">Cancer Molecular Biomarkers 2014: A Global Market Study</p>
<p class="product-content-text">Technological advances in molecular biology over the last decade are accelerating the development of new nucleic acid diagnostic tests These advances provide opportunities for faster, simpler and more...
</p>
<div class="visible-xs category-content">
	 <label>PUBLISHED </label>
	 <p class="publishe-date">Mar-14</p>
	 <p class="product-amount">USD 3,543</p>
	 
</div>
</td>
<td><p class="publishe-date">Mar-14</p></td>
<td><p class="product-amount">USD 3,543</p></td>
</tr>

<tr class="dark-background">

<td><p class="product-content-heading">
	 Research and Development in Biotechnology Industry (U.S.): Analytics, Extensive Financial Benchmarks, Metrics and Revenue Forecasts to 2020, NAIC 541711</p>
<p class="product-content-text">
	 Research and Development in Biotechnology Industry (U S ) : Analytics, Extensive Financial Benchmarks, Metrics and Revenue Forecasts to 2020, NAIC 541711 contains vital industry-specific data including...
</p>
<div class="visible-xs category-content">
<label>PUBLISHED </label>
<p class="publishe-date">Mar-14</p>
<p class="product-amount">USD 3,077 </p>
</div>
</td>
<td><p class="publishe-date">Mar-14</p></td>
<td><p class="product-amount">USD 3,077 </p></td>

</tr>
<tr  class="light-background">
<td>
<p class="product-content-heading">
Research and Development in the Physical, Engineering and Life Sciences Industry (U.S.): Analytics, Extensive Financial Benchmarks, Metrics and Revenue Forecasts to 2020, NAIC 541710</p>
<p class="product-content-text">
	 "Research and Development in the Physical, Engineering and Life Sciences Industry (U S ) : Analytics, Extensive Financial Benchmarks, Metrics and Revenue Forecasts to 2020, NAIC 541710 contains vital...
</p>
<div class="visible-xs category-content">
<label>PUBLISHED </label>
<p class="publishe-date">Mar-14</p>
<p class="product-amount">USD 1,995</p>
</div>
</td>
<td><p class="publishe-date">Mar-14</p></td>
<td><p class="product-amount">USD 1,995  </p><td>

</tr>
<tr  class="dark-background">
<td><p class="product-content-heading">
	 Biotechnology for the Non Biotechnologist (Course, France)</p>
<p class="product-content-text">
This comprehensive 3 day course is ideal for Scientists and Non-Scientists needing to understand the basic theory, principles and techniques of biotechnology TOPICS COVERED TO INCLUDE: - An introduction...
</p>
<div class="visible-xs category-content">
<label>PUBLISHED </label>
<p class="publishe-date">Feb-14</p>
<p class="product-amount">USD 1,995</p>
</div>
</td>
<td>
<p class="publishe-date">Feb-14</p>
</td>
<td><p class="product-amount">USD 1,995 </p>
</td>
</tr>

<tr  class="light-background">
<td>
<p class="product-content-heading">
Biological Identification. Woodhead Publishing Series in Electronic and Optical Materials</p>
<p class="product-content-text">
	 Biological identification systems are a vital component in countering the threats to life and health posed by natural pathogens, toxins, and bioterrorism agents. It is essential that such systems are...
</p>
<div class="visible-xs category-content">
<label>PUBLISHED </label>
<p class="publishe-date">Feb-14</p>
<p class="product-amount">USD 129 </p>
</div>
</td>
<td>
	 <p class="publishe-date">Feb-14</p></td>
<td><p class="product-amount">USD 129  </p></td>
</tr>
<tr  class="dark-background">
<td><p class="product-content-heading">
		  Cancer Molecular Biomarkers 2014: A Global Market Study</p>
	 <p class="product-content-text">
	 Technological advances in molecular biology over the last decade are accelerating the development of new nucleic acid diagnostic tests These advances provide opportunities for faster, simpler and more...
</p>
<div class="visible-xs category-content">
<label>PUBLISHED </label>
<p class="publishe-date">Feb-14</p>
<p class="product-amount">USD 2,150 </p>
</div>
</td>
<td>
	 <p class="publishe-date">Feb-14</p>
</td>
<td>
		  <p class="product-amount">USD 2,150</p>
</td>
</tr>
<tr class="light-background">
<td><p class="product-content-heading">
	 Research and Development in Biotechnology Industry (U.S.): Analytics, Extensive Financial Benchmarks, Metrics and Revenue Forecasts to 2020, NAIC 541711</p>
	 <p class="product-content-text">
	 Research and Development in Biotechnology Industry (U S ) : Analytics, Extensive Financial Benchmarks, Metrics and Revenue Forecasts to 2020, NAIC 541711 contains vital industry-specific data including...
</p>
<div class="visible-xs category-content">
<label>PUBLISHED </label>
<p class="publishe-date">Feb-14</p>
<p class="product-amount">USD 3,460</p>
</div>
</td>
<td>
<p class="publishe-date">Feb-14</p>
</td>
<td><p class="product-amount">USD 3,460</p></td>
</tr>

<tr class="dark-background">
<td><p class="product-content-heading">
		  Research and Development in the Physical, Engineering and Life Sciences Industry (U.S.): Analytics, Extensive Financial Benchmarks, Metrics and Revenue Forecasts to 2020, NAIC 541710</p>
	 <p class="product-content-text">
	 "Research and Development in the Physical, Engineering and Life Sciences Industry (U S ) : Analytics, Extensive Financial Benchmarks, Metrics and Revenue Forecasts to 2020, NAIC 541710 contains vital...
	 </p>
	 <div class="visible-xs category-content">
<label>PUBLISHED </label>
<p class="publishe-date">Feb-14</p>
<p class="product-amount">USD 230</p>
</div>
</td>
<td><p class="publishe-date">Feb-14</p></td>
<td><p class="product-amount">USD 230 </p></td>
</tr>

<tr class="light-background">
<td><p class="product-content-heading">
		  Biotechnology for the Non Biotechnologist (Course, France)</p>
<p class="product-content-text">
	 This comprehensive 3 day course is ideal for Scientists and Non-Scientists needing to understand the basic theory, principles and techniques of biotechnology TOPICS COVERED TO INCLUDE: - An introduction...

</p>
<div class="visible-xs category-content">
<label>PUBLISHED </label>
<p class="publishe-date">Jan-14 </p>
<p class="product-amount">USD 3,543</p>
</div>
</td>
<td><p class="publishe-date">Jan-14 </p></td>
<td><p class="product-amount">USD 3,543</p></td>
</tr>

<tr  class="dark-background">
<td>
<p class="product-content-heading">
		  Biological Identification. Woodhead Publishing Series in Electronic and Optical Materials</p>
<p class="product-content-text">
	 Biological identification systems are a vital component in countering the threats to life and health posed by natural pathogens, toxins, and bioterrorism agents. It is essential that such systems are...

</p>
<div class="visible-xs category-content">
<label>PUBLISHED </label>
<p class="publishe-date">Jan-14 </p>
<p class="product-amount">USD 3,460</p>
</div>
</td>
<td><p class="publishe-date">Jan-14 </p></td>
<td><p class="product-amount">USD 3,460</p></td>

</tr>

<tr  class="light-background">
<td>
<p class="product-content-heading">
		  Cancer Molecular Biomarkers 2014: A Global Market Study</p>
<p class="product-content-text">
	 Technological advances in molecular biology over the last decade are accelerating the development of new nucleic acid diagnostic tests These advances provide opportunities for faster, simpler and more...

</p>
<div class="visible-xs category-content">
<label>PUBLISHED </label>
<p class="publishe-date">Jan-14 </p>
<p class="product-amount">USD 230 </p>
</div>
</td>
<td><p class="publishe-date">Jan-14 </p></td>
<td><p class="product-amount">USD 230 </p></td>
</tr>

<tr class="dark-background">
<td>
<p class="product-content-heading">
		  Research and Development in Biotechnology Industry (U.S.): Analytics, Extensive Financial Benchmarks, Metrics and Revenue Forecasts to 2020, NAIC 541711</p>
<p class="product-content-text">
	 Research and Development in Biotechnology Industry (U S ) : Analytics, Extensive Financial Benchmarks, Metrics and Revenue Forecasts to 2020, NAIC 541711 contains vital industry-specific data including...

</p>
<div class="visible-xs category-content">
<label>PUBLISHED </label>
<p class="publishe-date">Jan-14 </p>
<p class="product-amount">USD 3,543 </p>
</div>
</td>
<td><p class="publishe-date">Jan-14 </p></td>
<td><p class="product-amount">USD 3,543 </p></td>

</tr>

<tr  class="light-background">
<td><p class="product-content-heading">
		  Research and Development in the Physical, Engineering and Life Sciences Industry (U.S.): Analytics, Extensive Financial Benchmarks, Metrics and Revenue Forecasts to 2020, NAIC 541710</p>
<p class="product-content-text">
	 "Research and Development in the Physical, Engineering and Life Sciences Industry (U S ) : Analytics, Extensive Financial Benchmarks, Metrics and Revenue Forecasts to 2020, NAIC 541710 contains vital...

</p>
<div class="visible-xs category-content">
<label>PUBLISHED </label>
<p class="publishe-date">Jan-14 </p>
<p class="product-amount">USD 3,077</p>
</div>
</td>
<td><p class="publishe-date">Jan-14 </p></td>
<td><p class="product-amount">USD 3,077</p></td>

</tr>
<!--main content end here-->
<tbody>
</table>
</div>
</div>
					 <!--content end here-->
					 
						  <p  class="pull-right black-text">page no <input type="text" class="form-control page-no" placeholder="1 ">
						  of 20 
								<span class="go-text">go</span>
								
								<span class="glyphicon glyphicon-menu-left filter-arrow" aria-hidden="true"></span><span class="glyphicon glyphicon-menu-right filter-arrow" aria-hidden="true"></span><div class="clearfix"></div>
						  </p>
						  <div class="adds-category hidden-sm hidden-xs pull-right">
								<a href="#"><img src="images/adds-category.jpg" alt="product-img" class="img-responsive adds-cat"></a>
						  </div>

				</div>

				<!--footer online icons-->
				<div class="row online-icon-section">
					 <div class="col-lg-6 col-sm-4 col-xs-6 pull-left ">
						  <div class="secured-img"><a href="#"><img src="images/secured-img.png" alt="secured-by" class="img-responsive"></a></div>
					 </div>
					 <div class="col-lg-6 col-sm-8 col-xs-12">
						  <div class="row">
								<div class="col-lg-7 col-md-6 col-sm-7 col-xs-8">
									 <p class="credit-card-text">We accept all major credit cards!</p>
									 <ul class="list-inline">
										  <li><a href="#"><img src="images/icon_1.jpg" alt="secured-by" class="img-responsive"></a></li>
										  <li><a href="#"><img src="images/icon_2.jpg" alt="secured-by" class="img-responsive"></a></li>
										  <li><a href="#"><img src="images/icon_3.jpg" alt="secured-by" class="img-responsive"></a></li>
										  <li><a href="#"><img src="images/icon_4.jpg" alt="secured-by"class="img-responsive"></a></li>
										  <li><a href="#"><img src="images/icon_5.jpg" alt="secured-by" class="img-responsive"></a></li>
									 </ul>
								</div>
								<div class="col-lg-5 col-md-6 col-sm-5 col-xs-4">
									 <p class="credit-card-text">No PayPal account required</p>
									 <div class="paypal-img">
										  <a href="#"><img src="images/icon_6.jpg" alt="secured-by" class="img-responsive"></a>
									 </div>
								</div>
						  </div>
					 </div>
				</div>
				<!--footer online icons end here -->
		  </div>
		  <!--end login here-->
		  
		<?php include("footer.php"); ?>
		<?php include("script.php"); ?>
		 <script>
				$('.alert-item').imagesLoaded( function() {
					 $('.alert-item').masonry({
						itemSelector: '.alert-box',
					 });

					 setTimeout(function() {
						  $('.alert-item').masonry('layout');
					 },500);
				});

		  </script>
		
	</body>
	<!-- Body end -->
</html>