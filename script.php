
<script src="js/bootstrap.min.js"></script>
<script src="js/imagesloaded.pkgd.min.js"></script>
<script src="js/masonry.pkgd.min.js"></script>
<script src="js/jquery.touchSwipe.min.js"></script>
<script src="js/script.js"></script>
<script>
$(document).ready(function(){
    $(".special-section-btn").click(function(){
        $(".special-section-toggle").slideToggle("fast");
    });
	$(".navbar-toggle").click(function() {
		$(".special-section-toggle").hide();
	});
	$(".toggle-search-box").click(function() {
		$(".search-box-header-mobile").slideToggle("fast");
	});
	$(".pause-icon").click(function() {
		$(".pause-icon").css("display","none");
		$(".play-icon").css("display","block");
	});
	$(".play-icon").click(function() {
		$(".play-icon").css("display","none");
		$(".pause-icon").css("display","block");
	});
	$(".search-icon").click(function() {
		$(".search-icon").css("display","none");
		$("i").css("display","block");
	});
	$("i").click(function() {
		$(".search-icon").css("display","block");
		$("i").css("display","none");
	});
});
</script>

