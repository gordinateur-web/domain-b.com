<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title>Login- New Customer| Domain-b </title>
      <?php include("head.php"); ?>
     
   </head>
   <body class="">
      <!-- Body start -->
      <?php include("header1.php"); ?>
     	 <!-- Breadcrumb Section start -->
			<div class="breadcrumb-section">
				<div class="container">
					<ol class="breadcrumb">
					  <li><a href="#">Home</a></li>
					  <li><a href="#">Login/Register</a></li>
					</ol>
				</div>
			</div>
		<!-- Breadcrumb section end -->

      <!--login window start here-->
	  	<div class="container">
	  		<div class="row">
	  			<h2 class="login2newcustomer-maintitle">Your login details</h2>
	  			<!--form start here-->
		  		<div class="col-lg-6 col-md-6 col-sm-6">
		  			<div class="clearfix">
		  			<p class="pull-right mendatory-text-login2">All fields are mendatory</p>
		  		</div>
		  			<div class="form-group">
					    <label class="control-label login2-new col-sm-4" for="email3">First name</label>
					    <div class="col-sm-8">
					        <input type="email" class="form-control login2-new" id="email3" placeholder="">
					    </div>
    				</div>
    				<div class="form-group">
					    <label class="control-label login2-new col-sm-4" for="email3">Last name</label>
					    <div class="col-sm-8">
					        <input type="email" class="form-control  login2-new" id="email3" placeholder="">
					    </div>
    				</div>
    				<div class="form-group">
					    <label class="control-label login2-new col-sm-4" for="email3">Job title</label>
					    <div class="col-sm-8">
					        <input type="email" class="form-control  login2-new" id="email3" placeholder="">
					    </div>
    				</div>
    				<div class="form-group">
					    <label class="control-label login2-new col-sm-4" for="email3">Organisation</label>
					    <div class="col-sm-8">
					        <input type="email" class="form-control  login2-new" id="email3" placeholder="">
					    </div>
    				</div>
    				<div class="form-group">
					    <label class="control-label login2-new col-sm-4" for="email3">Contact number</label>
					    <div class="col-sm-4">
					    		<select class="form-control image-drop" required>
						        <option value="disabled selected hidden">Country code</option>
						        <option value="0">12</option>
						        <option value="1">34</option>
						    </select>
						</div>

					    <div class="col-sm-4">
					        <input type="email" class="form-control login2-new" id="email3" placeholder="Phone number">
					    </div>
    				</div>
    				<div class="form-group">
    					<div class="col-sm-4">
					    	<label class="control-label login2-new" for="email3">Country</label>
						</div>
					    <div class="col-sm-8">
					    	<select class="form-control image-drop" required>
						        <option value="selected" >Select country</option>
						        <option value="0">India</option>
						        <option value="1">India</option>
						    </select>
						</div>
    				</div>
    				<button type="button" class="btn btn-default login2-new">Continue <span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span> </button>
    				
		  		</div>
		  		<!--form end here-->
		  	</div>

		  	<!--footer online icons-->
		  	<div class="row online-icon-section">
	  			<div class="col-lg-6 col-sm-4 col-xs-6 pull-left ">
	  				<div class="secured-img"><a href="#"><img src="images/secured-img.png" alt="secured-by" class="img-responsive"></a></div>
	  			</div>
	  			<div class="col-lg-6 col-sm-8 col-xs-12">
					<div class="row">
						<div class="col-lg-7 col-md-6 col-sm-7 col-xs-8">
							<p class="credit-card-text">We accept all major credit cards!</p>
							<ul class="list-inline">
								<li><a href="#"><img src="images/icon_1.jpg" alt="secured-by" class="img-responsive"></a></li>
								<li><a href="#"><img src="images/icon_2.jpg" alt="secured-by" class="img-responsive"></a></li>
								<li><a href="#"><img src="images/icon_3.jpg" alt="secured-by" class="img-responsive"></a></li>
								<li><a href="#"><img src="images/icon_4.jpg" alt="secured-by"class="img-responsive"></a></li>
								<li><a href="#"><img src="images/icon_5.jpg" alt="secured-by" class="img-responsive"></a></li>
							</ul>
						</div>
						<div class="col-lg-5 col-md-6 col-sm-5 col-xs-4">
							<p class="credit-card-text">No PayPal account required</p>
							<div class="paypal-img">
								<a href="#"><img src="images/icon_6.jpg" alt="secured-by" class="img-responsive"></a>
							</div>
						</div>
				  	</div>
				</div>
		  	</div>
		  	<!--footer online icons end here -->
	  	</div>
	  	<!--end login here-->
	  	
      <?php include("footer.php"); ?>
      <?php include("script.php"); ?>
	   <script>
			$('.alert-item').imagesLoaded( function() {
				$('.alert-item').masonry({
				  itemSelector: '.alert-box',
				});

				setTimeout(function() {
					$('.alert-item').masonry('layout');
				},500);
			});

		</script>
	  
   </body>
   <!-- Body end -->
</html>