<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title>Shopping Cart | Domain-b </title>
      <?php include("head.php"); ?>
     
   </head>
   <body class="">
      <!-- Body start -->
      <?php include("header1.php"); ?>
     	 <!-- Breadcrumb Section start -->
			<div class="breadcrumb-section">
				<div class="container">
					<ol class="breadcrumb">
					  <li><a href="#">Home ></li>
					  <li><a href="#">Shopping Cart</a></li>
					</ol>
				</div>
			</div>
		<!-- Breadcrumb section end -->

      <!--login window start here-->
	  	<div class="container push-footer">
	  		<h2 class="shopping-cart-title">Shopping cart</h2>
	  			
		  		<div class="row shopping-cart">
		  			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		  			<!--product detail section start here-->
		  			<div class="table-responsive">          
						  <table class="table shopping-cart">
						    <thead class="heading-section">
						      <tr class="product-title">
						        <th class="col-lg-1"></th>
						        <th class="col-lg-6">product</th>
						        <th class="col-lg-2 text-center">quantity</th>
						        <th class="text-right col-lg-3"><span class="text-price">price</spam></th>
						       </tr>
						    </thead>
						    <tbody class="product-details-section">
						      <tr>
						        <td class="text-center"><input type="checkbox"  name="subscribe" value="newsletter"></td>
						        <td>
						        	<p class="product-detail">Biological Identification. Woodhead Publishing Series in Electronic and Optical Materials</p>
						        	<p class="user-text">Single user</p>
						    	</td>
						        <td><select class="form-control quantity" required>
							      <option value="selected">1</option>
							        <option value="0">2</option>
							        <option value="1">3</option>
			
							    </select></td>
						        <td><p class="product-price">US$ 3,950.00</p></td>
						     </tr>
						    </tbody>
						    <tbody class="product-details-section">
						      <tr>
						        <td class="text-center"><input type="checkbox" name="subscribe" value="newsletter"></td>
						        <td><p class="product-detail">Cancer Molecular Biomarkers 2014: A Global Market Study</p>
						        <p class="user-text">Single user</p></td>
						        <td><select class="form-control quantity" required>
							        <option value=" selected hidden">1</option>
							        <option value="0">2</option>
							        <option value="1">3</option>
							    </select></td>
						        <td><p class="product-price">US$ 2,150.00</p></td>
						     </tr>
						    </tbody>
						  </table>
					 </div>
					  <!--product detail section end here-->
					 </div>
		  		</div>
		  		<div class="row">
		  			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			  			<h5 class="fianl-cost pull-right cart-page">Current total: <strong>US$ 6,100.00</strong></h5>
			  		</div>
			  	</div>
		  		<div class="row">
		  			<!--buttons start here-->
			  		<div class="col-lg-offset-6 col-lg-3 col-md-offset-6 col-md-3 col-sm-offset-6 col-sm-3 col-xs-6">
			  			<button type="button" class="btn btn-default shopping-cart"><span class="glyphicon glyphicon-menu-left" aria-hidden="true"></span> Continue shopping</button>
			  		</div>
			  		<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
			  			<button type="button" class="btn btn-default shopping-cart2">proceed to checkout <span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span> </button>
			  		</div>
			  		<!--buttons end here-->
		  		</div>


		  	<!--footer online icons-->
		  	<div class="row online-icon-section">
	  			<div class="col-lg-6 col-sm-4 col-xs-6 pull-left ">
	  				<div class="secured-img"><a href="#"><img src="images/secured-img.png" alt="secured-by" class="img-responsive"></a></div>
	  			</div>
	  			<div class="col-lg-6 col-sm-8 col-xs-12">
					<div class="row">
						<div class="col-lg-7 col-md-6 col-sm-7 col-xs-8">
							<p class="credit-card-text">We accept all major credit cards!</p>
							<ul class="list-inline">
								<li><a href="#"><img src="images/icon_1.jpg" alt="secured-by" class="img-responsive"></a></li>
								<li><a href="#"><img src="images/icon_2.jpg" alt="secured-by" class="img-responsive"></a></li>
								<li><a href="#"><img src="images/icon_3.jpg" alt="secured-by" class="img-responsive"></a></li>
								<li><a href="#"><img src="images/icon_4.jpg" alt="secured-by"class="img-responsive"></a></li>
								<li><a href="#"><img src="images/icon_5.jpg" alt="secured-by" class="img-responsive"></a></li>
							</ul>
						</div>
						<div class="col-lg-5 col-md-6 col-sm-5 col-xs-4">
							<p class="credit-card-text">No PayPal account required</p>
							<div class="paypal-img">
								<a href="#"><img src="images/icon_6.jpg" alt="secured-by" class="img-responsive"></a>
							</div>
						</div>
				  	</div>
				</div>
		  	</div>
		  	<!--footer online icons end here -->
	  	</div>
	  	<!--end login here-->
	  	
      <?php include("footer.php"); ?>
      <?php include("script.php"); ?>
	   <script>
			$('.alert-item').imagesLoaded( function() {
				$('.alert-item').masonry({
				  itemSelector: '.alert-box',
				});

				setTimeout(function() {
					$('.alert-item').masonry('layout');
				},500);
			});

		</script>
	  
   </body>
   <!-- Body end -->
</html>