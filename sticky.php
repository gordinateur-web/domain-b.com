<div class="sticky-container hidden-xs">
	<ul class="list-unstyled sticky">
		<li class="facebook"><a href=""><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
		<li class="linkdin"><a href=""><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
		<li class="twitter"><a href=""><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
		<li class="print"><a href=""><i class="fa fa-print" aria-hidden="true"></i></a></li>
		<li class="plus"><a href=""><i class="fa fa-plus" aria-hidden="true"></i></a></li>
	</ul>
</div>