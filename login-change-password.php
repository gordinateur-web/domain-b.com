<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title>Login-Register | Domain-b </title>
      <?php include("head.php"); ?>
     	<!-- include jQuery for tabs-->
		<script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
		<!-- include jQuery UI for tabs-->
		<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
   </head>
   <body class="">
      <!-- Body start -->
      <?php include("header1.php"); ?>
      <!-- Breadcrumb Section start -->
	<div class="breadcrumb-section">
		<div class="container">
			<ol class="breadcrumb">
			  <li><a href="#">Home</a></li>
			  <li><a href="#">Login/register</a></li>
			</ol>
		</div>
	</div>

<!-- Breadcrumb section end -->
      <!--login window start here-->
	  	<div class="container push-footer">
	  		<div class="row">
	  			<div class="col-xs-12 col-sm-12 col-lg-12">
      				<div class="draggable-container">
						<ul class="nav nav-tabs draggable  login-profile" role="tablist">
						    <li role="presentation" ><a href="#" role="tab">Your profile details</a></li>
						    <li role="presentation" ><a  href="#" role="tab" >Edit profile</a></li>
						    <li  role="presentation" class="active"><a href="#" role="tab" data-toggle="tab">Change password</a></li>
						    <li role="presentation" ><a href="#" role="tab">Order history</a></li>
						    <li role="presentation" ><a  href="#" role="tab">Logout</a></li>
						 </ul>
					</div>
				</div>

				<div class="tab-content profile-page">
					<div class="row">
						<div class="col-lg-6 col-md-6 col-sm-6">
							<h2 class="login-maintitle tab-heading">Change password</h2>
				  			<div class="form-group">
							    <label class="control-label your-details col-sm-4" >Old Password</label>
							    <div class="col-sm-8">
							        <input type="email" class="form-control  your-details" id="email3" placeholder="">
							    </div>
		    				</div>
				  			<div class="form-group">
							    <label class="control-label your-details col-sm-4" >New Password</label>
							    <div class="col-sm-8">
							        <input type="email" class="form-control  your-details" id="email3" placeholder="">
							    </div>
		    				</div>
				  			<div class="form-group">
							    <label class="control-label your-details col-sm-4" >Confirm Password</label>
							    <div class="col-sm-8">
							        <input type="email" class="form-control  your-details" id="email3" placeholder="">
							    </div>
		    				</div>
		    				<div class="form-group">
							    <label class="control-label your-details col-sm-4" ></label>
							    <div class="col-sm-8">
							       <button type="button" class="btn btn-default brown-button change-password">Update<span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span></button>
							    </div>
		    				</div>
		    				
				  		</div>
					</div>
				</div>
			</div>

		  	<!--footer online icons-->
		  	<div class="row online-icon-section">
	  			<div class="col-lg-6 col-sm-4 col-xs-6 pull-left ">
	  				<div class="secured-img"><a href="#"><img src="images/secured-img.png" alt="secured-by" class="img-responsive"></a></div>
	  			</div>
	  			<div class="col-lg-6 col-sm-8 col-xs-12">
					<div class="row">
						<div class="col-lg-7 col-md-6 col-sm-7 col-xs-8">
							<p class="credit-card-text">We accept all major credit cards!</p>
							<ul class="list-inline">
								<li><a href="#"><img src="images/icon_1.jpg" alt="secured-by" class="img-responsive"></a></li>
								<li><a href="#"><img src="images/icon_2.jpg" alt="secured-by" class="img-responsive"></a></li>
								<li><a href="#"><img src="images/icon_3.jpg" alt="secured-by" class="img-responsive"></a></li>
								<li><a href="#"><img src="images/icon_4.jpg" alt="secured-by"class="img-responsive"></a></li>
								<li><a href="#"><img src="images/icon_5.jpg" alt="secured-by" class="img-responsive"></a></li>
							</ul>
						</div>
						<div class="col-lg-5 col-md-6 col-sm-5 col-xs-4">
							<p class="credit-card-text">No PayPal account required</p>
							<div class="paypal-img">
								<a href="#"><img src="images/icon_6.jpg" alt="secured-by" class="img-responsive"></a>
							</div>
						</div>
				  	</div>
				</div>
		  	</div>
		  	<!--footer online icons end here -->
	  	</div>
	  	<!--end login here-->
      <?php include("footer.php"); ?>
      <?php include("script.php"); ?>
      <!--add js for tabs -->
      <script src="js/tabs.js"></script>
	   <script>
			$('.alert-item').imagesLoaded( function() {
				$('.alert-item').masonry({
				  itemSelector: '.alert-box',
				});

				setTimeout(function() {
					$('.alert-item').masonry('layout');
				},500);
			});

		</script>
	  
   </body>
   <!-- Body end -->
</html>