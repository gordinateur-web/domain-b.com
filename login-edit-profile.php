<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title>Payment Details | Domain-b </title>
      <?php include("head.php"); ?>
     	<!-- include jQuery for tabs-->
		<script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
		<!-- include jQuery UI for tabs-->
		<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
   </head>
   <body class="">
      <!-- Body start -->
      <?php include("header1.php"); ?>
     	  <!-- Breadcrumb Section start -->
			<div class="breadcrumb-section">
				<div class="container">
					<ol class="breadcrumb">
					  <li><a href="#">Home</a></li>
					  <li><a href="#">Login/register</a></li>
					</ol>
				</div>
			</div>

		<!-- Breadcrumb section end -->
		 <!--login window start here-->
	  	<div class="container">
	  		<div class="row">
	  				<div class="col-xs-12 col-sm-12 col-lg-12">
	      				<div class="draggable-container">
							<ul class="nav nav-tabs draggable  login-profile" role="tablist">
							    <li role="presentation" ><a href="#" role="tab">Your profile details</a></li>
							    <li role="presentation" class="active"><a  href="#" role="tab" data-toggle="tab">Edit profile</a></li>
							    <li  role="presentation" ><a href="#" role="tab" >Change password</a></li>
							    <li role="presentation" ><a href="#" role="tab">Order history</a></li>
							    <li role="presentation" ><a  href="#" role="tab">Logout</a></li>
							 </ul>
						</div>
					</div>
					<div class="tab-content profile-page">
					  	<div class="row">
					  		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
							 		<h2 class="login-maintitle tab-heading">Edit your profile</h2>
								   <div class="form-group">
									    <label class="control-label your-details col-sm-4" >First name</label>
									    <div class="col-sm-8">
									        <input type="email" class="form-control  your-details" id="email3" placeholder="Lokesh">
									    </div>
				    				</div>
				    				<div class="form-group">
									    <label class="control-label your-details col-sm-4" >Last name</label>
									    <div class="col-sm-8">
									        <input type="email" class="form-control  your-details" id="email3" placeholder="Thakur">
									    </div>
				    				</div>
				    				<div class="form-group">
									    <label class="control-label your-details col-sm-4" >Job title</label>
									    <div class="col-sm-8">
									        <input type="email" class="form-control  your-details"  placeholder="Mng">
									    </div>
				    				</div>
				    				<div class="form-group">
									    <label class="control-label your-details col-sm-4" >Organisation</label>
									    <div class="col-sm-8">
									        <input type="email" class="form-control  your-details" id="email3" placeholder="TIC">
									    </div>
				    				</div>
				    				<div class="form-group">
									    <label class="control-label your-details col-sm-4" >Email</label>
									    <div class="col-sm-8">
									        <input type="email" class="form-control  your-details" id="email3" placeholder="lokesh.thakur@ticworks.com">
									    </div>
				    				</div>
				    				<div class="form-group">
									    <label class="control-label your-details col-sm-4" >Contact number</label>
									    <div class="col-sm-3">
									    		<select class="form-control" required>
										        <option value="disabled selected hidden">+91</option>
										        <option value="0">12</option>
										        <option value="1">34</option>
										    </select>
										</div>

									    <div class="col-sm-5">
									        <input type="email" class="form-control your-details" id="email3" placeholder="Phone number">
									    </div>
				    				</div>
				    				
				    				<div class="clearfix"></div>

				    				<div class="form-group">
									  	<label class="control-label your-details col-sm-4">Address</label>
									   	<div class="col-sm-8">
									  	<textarea class="form-control" rows="5" ></textarea>
									  </div>
									</div>

				    				<div class="form-group">
									    <label class="control-label your-details col-sm-4" >City</label>
									    <div class="col-sm-8">
									        <input type="email" class="form-control  your-details"  placeholder="Navi mumbai">
									    </div>
				    				</div>
				    				<div class="form-group">
				    					<label class="control-label your-details col-sm-4">Country</label>
										<div class="col-sm-8">
									    	<select class="form-control" required>
									    	<option value="selected">Select country</option>
										        <option value="0">India</option>
										        <option value="1">India</option>
										    </select>
										</div>
				    				</div>

				    				<div class="clearfix"></div>
				    				<div class="form-group">
									    <label class="control-label your-details col-sm-4" ></label>
									    <div class="col-sm-8">
									       <button type="button" class="btn btn-default brown-button change-password">Update<span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span></button>
										</div>
						    		</div>
							</div>
					   
					</div>
				</div>
			</div>	
     		

		  	<!--footer online icons-->
		  	<div class="row online-icon-section">
	  			<div class="col-lg-6 col-sm-4 col-xs-6 pull-left ">
	  				<div class="secured-img"><a href="#"><img src="images/secured-img.png" alt="secured-by" class="img-responsive"></a></div>
	  			</div>
	  			<div class="col-lg-6 col-sm-8 col-xs-12">
					<div class="row">
						<div class="col-lg-7 col-md-6 col-sm-7 col-xs-8">
							<p class="credit-card-text">We accept all major credit cards!</p>
							<ul class="list-inline">
								<li><a href="#"><img src="images/icon_1.jpg" alt="secured-by" class="img-responsive"></a></li>
								<li><a href="#"><img src="images/icon_2.jpg" alt="secured-by" class="img-responsive"></a></li>
								<li><a href="#"><img src="images/icon_3.jpg" alt="secured-by" class="img-responsive"></a></li>
								<li><a href="#"><img src="images/icon_4.jpg" alt="secured-by"class="img-responsive"></a></li>
								<li><a href="#"><img src="images/icon_5.jpg" alt="secured-by" class="img-responsive"></a></li>
							</ul>
						</div>
						<div class="col-lg-5 col-md-6 col-sm-5 col-xs-4">
							<p class="credit-card-text">No PayPal account required</p>
							<div class="paypal-img">
								<a href="#"><img src="images/icon_6.jpg" alt="secured-by" class="img-responsive"></a>
							</div>
						</div>
				  	</div>
				</div>
		  	</div>
		  	<!--footer online icons end here -->
	  	</div>
	  	<!--end login here-->
	  	
      <?php include("footer.php"); ?>
      <?php include("script.php"); ?>
      <!--add js for tabs -->
       <script src="js/tabs.js"></script>
	   <script>
			$('.alert-item').imagesLoaded( function() {
				$('.alert-item').masonry({
				  itemSelector: '.alert-box',
				});

				setTimeout(function() {
					$('.alert-item').masonry('layout');
				},500);
			});

		</script>
	  
   </body>
   <!-- Body end -->
</html>