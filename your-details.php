<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title>Your Details | Domain-b </title>
      <?php include("head.php"); ?>
     
   </head>
   <body class="">
      <!-- Body start -->
      <?php include("header1.php"); ?>
     	 <!-- Breadcrumb Section start -->
			<div class="container">
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 hidden-xs text-center">
						<div class="breadcrumb-nav flat">
							<a href="#" class="active">CHECKOUT</a>
							<a href="#" class="active"> YOUR DETAILS  </a>
							<a href="#" class="grey-arrow">PAYMENT</a>
							<a href="#">CONFIRMATION</a>
						</div>
					</div>
				</div>
			</div>
		<!-- Breadcrumb section end -->

      <!--login window start here-->
	  	<div class="container">
	  		<div class="row">
	  			<h2 class="login-maintitle">Your login details</h2>
	  			<!--form start here-->
		  		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		  			
		  			<div class="clearfix">
		  			<p class="pull-right mendatory-text">All fields are mendatory</p>
		  			</div>
		  			<div class="form-group">
					    <label class="control-label your-details col-sm-4" for="email3">First name</label>
					    <div class="col-sm-8">
					        <input type="email" class="form-control  your-details" id="email3" placeholder="">
					    </div>
    				</div>
    				<div class="form-group">
					    <label class="control-label your-details col-sm-4" for="email3">Last name</label>
					    <div class="col-sm-8">
					        <input type="email" class="form-control  your-details" id="email3" placeholder="">
					    </div>
    				</div>
    				<div class="form-group">
					    <label class="control-label your-details col-sm-4" for="email3">Job title</label>
					    <div class="col-sm-8">
					        <input type="email" class="form-control  your-details" id="email3" placeholder="">
					    </div>
    				</div>
    				<div class="form-group">
					    <label class="control-label your-details col-sm-4" for="email3">Organisation</label>
					    <div class="col-sm-8">
					        <input type="email" class="form-control  your-details" id="email3" placeholder="">
					    </div>
    				</div>
    				<div class="form-group">
					    <label class="control-label your-details col-sm-4" for="email3">Contact number</label>
					    <div class="col-sm-4">
					    		<select class="form-control image-drop" required>
						        <option value="disabled selected hidden">Country code</option>
						        <option value="0">12</option>
						        <option value="1">34</option>
						    </select>
						</div>

					    <div class="col-sm-4">
					        <input type="email" class="form-control your-details" id="email3" placeholder="Phone number">
					    </div>
    				</div>
    				<div class="form-group">
    					<label class="control-label your-details col-sm-4" for="email3">Country</label>
						<div class="col-sm-8">
					    	<select class="form-control image-drop" required>
					    	<option value="selected">Select country</option>
						        <option value="0">India</option>
						        <option value="1">India</option>
						    </select>
						</div>
    				</div>
    				<button type="button" class="btn btn-default your-details">Continue <span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span> </button>
    				
		  		</div>
		  		<!--form end here-->
		  		<!--order summary section start here-->
		  		<div class="col-lg-6 col-md-6 col-sm-12 col-sm-6 col-xs-12">
		  				<div class="summary-details-title"><h3>Order Summary</h3></div>
		  				<div class="row summary-blog">
		  					<div class="col-lg-2 col-sm-2 col-xs-3">
		  						<div class="cross-img"><img src="images/cross-img.png" alt="remove-img" class="img-responsive"></div>
			  					<p class="summary-remove-text">Remove</p>
			  				</div>
		  					<div class="col-lg-6 col-sm-7 col-xs-6">
		  						<p class="summary-details-text">
		  							Biological Identification. Woodhead Publishing Series in Electronic and Optical Materials
								</p>
		  						<p class="summary-user">Single user | Qty:1 </p>
		  					</div>
		  					<div class="col-lg-4 col-sm-3 col-xs-3">
		  						<p class="summary-count">US$ 3,950.00</p>
		  					</div>
		  				</div>
		  				<div class="row summary-blog">
		  					<div class="col-lg-2 col-sm-2 col-xs-3">
		  						<div class="cross-img"><img src="images/cross-img.png" alt="remove-img" class="img-responsive"></div>
		  						<p class="summary-remove-text">Remove</p>
		  					</div>
		  					<div class="col-lg-6 col-sm-7 col-xs-6">
		  						<p class="summary-details-text">Cancer Molecular Biomarkers 2014:<br/>
		  						A Global Market Study</p>
		  						<p class="summary-user">Single user | Qty:1 </p>
		  					</div>
		  					<div class="col-lg-4 col-sm-3 col-xs-3">
		  						<p class="summary-count">US$ 2,150.00</p>
		  					</div>
		  				</div>
		  				<div class="row">
		  					<div class="col-lg-12 col-md-12 col-xs-12">
		  						<div class=" summary-total">
		  						<p>Current total: <strong>US$ 6,100.00</strong></p>
		  					</div>
		  					</div>
		  				</div>
		  				
		  		</div>
		  		<!--order summary section end here-->
		  	</div>

		  	<!--footer online icons-->
		  	<div class="row online-icon-section">
	  			<div class="col-lg-6 col-sm-4 col-xs-6 pull-left ">
	  				<div class="secured-img"><a href="#"><img src="images/secured-img.png" alt="secured-by" class="img-responsive"></a></div>
	  			</div>
	  			<div class="col-lg-6 col-sm-8 col-xs-12">
					<div class="row">
						<div class="col-lg-7 col-md-6 col-sm-7 col-xs-8">
							<p class="credit-card-text">We accept all major credit cards!</p>
							<ul class="list-inline">
								<li><a href="#"><img src="images/icon_1.jpg" alt="secured-by" class="img-responsive"></a></li>
								<li><a href="#"><img src="images/icon_2.jpg" alt="secured-by" class="img-responsive"></a></li>
								<li><a href="#"><img src="images/icon_3.jpg" alt="secured-by" class="img-responsive"></a></li>
								<li><a href="#"><img src="images/icon_4.jpg" alt="secured-by"class="img-responsive"></a></li>
								<li><a href="#"><img src="images/icon_5.jpg" alt="secured-by" class="img-responsive"></a></li>
							</ul>
						</div>
						<div class="col-lg-5 col-md-6 col-sm-5 col-xs-4">
							<p class="credit-card-text">No PayPal account required</p>
							<div class="paypal-img">
								<a href="#"><img src="images/icon_6.jpg" alt="secured-by" class="img-responsive"></a>
							</div>
						</div>
				  	</div>
				</div>
		  	</div>
		  	<!--footer online icons end here -->
	  	</div>
	  	<!--end login here-->
	  	
      <?php include("footer.php"); ?>
      <?php include("script.php"); ?>
	   <script>
			$('.alert-item').imagesLoaded( function() {
				$('.alert-item').masonry({
				  itemSelector: '.alert-box',
				});

				setTimeout(function() {
					$('.alert-item').masonry('layout');
				},500);
			});

		</script>
	  
   </body>
   <!-- Body end -->
</html>