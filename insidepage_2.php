<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
      <title>Home | Domain-b </title>
      <?php include("head.php"); ?>
      <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
      <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->
   </head>
   <body class="">
      <?php include("header.php"); ?>
	   <?php include("sticky.php"); ?>
<!-- Breadcrumb Section start -->
	<div class="breadcrumb-section">
		<div class="container">
			<ol class="breadcrumb">
			  <li><a href="#">Industry</a></li>
			  <li><a href="#">Industry monitor</a></li>
			  <li><a href="#">Telecom</a></li>
			</ol>
			<p>More reports on: <a href="">M&A</a></p>
		</div>
	</div>

<!-- Breadcrumb section end -->
 <!-- Breaking News Start -->
      <div class="breaking-news">
         <div class="container">
            <div class="row">
               <div class="col-lg-9 col-md-10 col-sm-10 col-xs-12">
                  <h4>
                     <span><a href="">BREAKING NEWS:</a></span>
                     <marquee class="marquee" behavior="scroll" direction="left" id="mymarquee">
                        <a href="">Australian prime minister Julia Gillard throws open leadership to an expected challenge by Kevin Rudd.</a>
                     </marquee>
                     <img src="images/double-bar.png"  class="img-responsive-pull-right pause-icon" value="Stop Marquee" onClick="document.getElementById('mymarquee').stop();">
                     <i class="fa fa-play pull-right play-icon" aria-hidden="true" value="Start Marquee" onClick="document.getElementById('mymarquee').start();"></i>
                  </h4>
               </div>
            </div>
         </div>
      </div>
    <!-- Breaking News End -->
	<div class="insidepage-container">
		<div class="container">
			<div class="row">
				<div class="col-lg-10 col-md-10 col-sm-10 col-xs-12 insidepage-lhs remove-padding-left">
					<div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
						<article>
							<h2>2G spectrum auction begins amid strong bidding for all circles</h2>
							<p  class="article-date">03 February 2014</p>
							<img src="images/article-img-1.jpg" class="img-responsive">
							<p>With eight companies in the fray for GSM and CDMA spectrum, bidding in the 2G spectrum auction got under way today this morning with bids coming in for all 22 circles in the 1,800 MHz band and all three zones in 900 MHz band.</p>
							<p>2G spectrum auction begins amid strong bidding for all circles In contrast to the auction in March last year which was a damp affair, this time the bids have poured in from eight telecom companies, including market leaders Bharti Airtel, Vodafone India, and Reliance Jio Infocomm, after the government reduced the base, or minimum bid, price to more realistic levels. The government expects to garner at least Rs11,300 crore from this round of auctions.</p>
							<p>"Till now, bids have been received in all 22 service areas in 1800 MHz band and in all three service areas in 900 MHz band. Thus, bidding has taken place in all the service areas in both bands," an official statement said.</p>
							<p>The auction started as planned after the Supreme Court on Saturday refused to interfere with the order of the telecom tribunal TDSAT dismissing the pleas of Bharti Airtel, Vodafone, Loop Telecom and Idea Telecom for a stay on the spectrum auction and extension of their licenses by another 10 years.</p>
							<p>The department of telecommunications (DoT) has put on the block about 385 MHz of radio waves in the 1800 MHz band and 46 MHz in the 900 MHz band.</p>
							<p>Normally, six to seven rounds of bidding take place in a day with each round of 60 minutes. There is a 20-minute break between rounds. Four rounds of bidding had been completed at the time of writing today.</p>
							<p>The government is conducting this third round of spectrum auction following a Supreme Court order in the 2G case directing that all radio waves freed from the cancellation of 122 licences in February 2012 should be auctioned.</p>
							<p>Besides, the 900 Mhz has to be auctioned as some of the spectrum in this band is held under old telecom licences, which will start expiring from November 2014.</p>
							<p><img src="images/rock.jpg" class="img-responsive pull-right">The 3G auction in 2010  lasted for 34 days, while the offer in November 2012 lasted for two days and the one in March last year lasted for just one day. In November 2012, Rs9,407 crore of bids were received for spectrum worth Rs28,000 that was offered. In March 2013, no GSM operators bid at the auction and only CDMA operator Sistema Shyam bought spectrum of about Rs3,600 crore in eight of the 21 service areas.</p>
							<p>Vodafone needs to get spectrum in Delhi, Mumbai and Kolkata; Airtel in Delhi and Mumbai, and Loop Mobile in Mumbai to continue operations as their licences expire in November.</p>
						</article>
						<div class="latest-domain-b-report">
							<h2>Related Reports</h2>
							<!--related reports ul start -->
							<ul class="list-unstyled">
								<li><a href="">Bharti Airtel, Vodafone among 8 in race for 2G spectrum</a></li>
								<li><a href="">Supreme Court refuses to stay spectrum auction</a></li>
								<li><a href="">Cabinet finalises 5% usage charge for spectrum in 1800 MHz and 900 MHz bands</a></li>
								<li><a href="">Netherlands tells ISPs to unblock The Pirate Bay</a></li>
								<li><a href="">US to allow firms to disclose data on NSA requests</a></li>
							</ul>
							<!-- related reports ul end -->
							<div class="red-button">
								<a href="" class="btn btn-default">MORE REPORTS</a>
							</div>
						</div>
						<!-- Advertise section start -->
						<section class="advertise-section">
							<a href="#"><img src="images/advertise-img.jpg" class="img-responsive"></a>
							<!-- <div class="">
								<h2>IndiaMART-B2B Marketplace</h2>
								<p><a href="">indiamart.com</a></p>
								<p>Find right supplier on Indiamart Search from 14 lakh suppliers</p>
							</div>
							<div class="advertise-article-discussion">
								<p>Discuss this article</p>
								<ul class="list-unstyled">
									<li><a href="">Write a comment</a></li>
									<li><a href="">Read guid lines for participation</a></li>
								</ul>
							</div> -->
						</section>
						<!-- Advertise section end -->
					</div>
					<div class="col-lg-offset-1 col-md-offset-1 col-lg-4 col-md-4 col-sm-5 col-xs-12">
						<div class="latest-domain-b-report">
							<h2>Latest domain-b reports</h2>
							<!--latest domain-b reports ul start -->
							<ul class="list-unstyled">
								<li><a href="">Lupin acquires Dutch injectable company Nanomi BV</a></li>
								<li><a href="">Nestle mulls selling Davigel frozen foods business</a></li>
								<li><a href="">Engineers India FPO to open on 6 February</a></li>
								<li><a href="">India cuts jet fuel prices 3% on back of lower import costs</a></li>
								<li><a href="">Valeant Pharmaceuticals to buy PreCision Dermatology for $475 mn</a></li>
								<li><a href="">Nestle to sell sports nutrition PowerBar and Musashi brands to Post Holdings</a></li>
								<li><a href="">Steelmakers, miners at war over iron ore 'cartelisation'</a></li>
								<li><a href="">MFs must work to regain lost public trust: Chidambaram</a></li>
								<li><a href="">Indo-Russian BrahMos missile to be ready for air tests by year-end</a></li>
								<li><a href="">Cells from the dead may help the blind to</a></li>
							</ul>
							<!-- latest domain-b reports ul end -->
							<div class="red-button">
								<a href="" class="btn btn-default">MORE REPORTS</a>
							</div>
						</div>
						<div class="">
							<img src="images/cemera-sell.jpg" class="img-responsive">
						</div>
						<!--Market resereach reports section start -->
						<div class="resereach-reports-section">
							<h2>buy market research reports</h2>
							<div class="resereach-reports-listing">
								<h3>Reports by country</h3>
								<select id="select-country">
								  <option value="Select Country">Select Country</option>
								  <option value="">India</option>
								  <option value="">Usa</option>
								  <option value="">Austrailiya</option>
								</select>
								<h3>Reports by Sectors</h3>
								<ul class="list-unstyled">
									<li><a href="">- Pharmaceuticals and Healthcare</a></li>
									<li><a href="">- Business and Finance</a></li>
									<li><a href="">- Consumer and Retail</a></li>
									<li><a href="">- Manufacturing and Construction</a></li>
									<li><a href="">- Energy and Transport</a></li>
									<li><a href="">- Telecommunications and Computing</a></li>
									<li><a href="">- Government and Public Sector</a></li>
									<li><a href="">- Company Reports</a></li>
									<li><a href="">- Country Reports</a></li>
									<li><a href="">- Industry Standards</a></li>
									<li><a href="">- Science</a></li>
									<li><a href="">- Humanities</a></li>
									<li class="text-right browse-all-reports"><a href="">Browse all sector reports <i class="fa fa-caret-right" aria-hidden="true"></i><i class="fa fa-caret-right" aria-hidden="true"></i></a></li>
								</ul>
							</div>
							<div class="resereach-reports-bottom-section">
								<img src="images/thawte.jpg" class="img-responsive">
							</div>
						</div>
						<!--Market resereach reports section end -->
						<!-- Domain-b-shopping start -->
						<div class="domain-b-shopping-rhs domain-b-shopping-border">
							<div class="domain-b-shopping-header domain-b-shopping-header-gap">
								<img src="images/domain-b-shopping.jpg" class="img-responsive domain-b-shopping-img">
							</div>
							<div class="">
							 <img src="images/product-img3.jpg" class="img-responsive img-center">
							 <p class="explore-more-products"><a href="" class="pull-right">explore more<i class="fa fa-caret-right" aria-hidden="true"></i><i class="fa fa-caret-right" aria-hidden="true"></i></a></p>
						  </div>
						  <div class="">
							 <img src="images/product-img4.jpg" class="img-responsive img-center">
							 <p class="explore-more-products"><a href="" class="pull-right">explore more<i class="fa fa-caret-right" aria-hidden="true"></i><i class="fa fa-caret-right" aria-hidden="true"></i></a></p>
						  </div>
					</div>
					<!-- Domain-b-shopping end -->
					</div>
				</div>
				<div class="col-lg-2 col-md-2 col-sm-2 col-xs-12 insidepage-rhs iframe-section1">
					<div class="iframe-img-listing">
                      <img src="images/currency-converter.jpg" class="img-responsive img-center">
                   </div>
					<div class="iframe-img-listing">
                      <img src="images/smg-rocks.jpg" class="img-responsive img-center">
                   </div>
                   <div class="iframe-img-listing">
                      <img src="images/icecream.jpg" class="img-responsive img-center">
                  </div>
                  <div class="iframe-img-listing">
                     <img src="images/mela.jpg" class="img-responsive img-center">
                  </div>
				</div>
			</div>
		</div>
	</div>
      <?php include("footer.php"); ?>
      <?php include("script.php"); ?>
   </body>
</html>